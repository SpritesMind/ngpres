#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

#define HAVE_INTTYPES_H
#define HAVE_STDINT_H
#include "StSoundLibrary.h"

#include "genres.h"
#include "utils/file_utils.h"
#include "utils/sound_utils.h"
#include "utils/parser.h"

#define AFX_TYPE "afx"
#define YM_TYPE  "ym"
#define YM3_TYPE "ym3"
#define YM5_TYPE "ym5"
#define YM6_TYPE "ym6"

#define AY_BIT_VOLUME_VALUE	    0x0F
#define AY_BIT_TONE_DISABLE	    0x10
#define AY_BIT_TONE_CHANGE	    0x20
#define AY_BIT_NOISE_CHANGE	    0x40
#define AY_BIT_NOISE_DISABLE    0x80

#define DEST_PAL		3546893
#define DEST_NTSC		3579545

#define SOURCE_AUTO		9999999
#define SOURCE_AYFX     4000000
#define SOURCE_AMSTRAD	1000000
#define SOURCE_ATARI	2000000
#define SOURCE_INTELLIVISION    1789772
#define SOURCE_MSX		2000000
#define SOURCE_SPECCY	1773400
#define SOURCE_VECTREX  1500000

#define CHAN_DISABLED	9

#define SET_BIT(n,b)	( (n) |= (1 << (b)) )
#define CLEAR_BIT(n,b)	( (n) &= ~(1 << (b)) )
#define GET_BIT(n,b)	(((n) >> (b)) & 1)

#define MIXER_BIT_NOISE 3

#define SET_VOLUME(c, v)		(0x80 | ((c) << 5) | 0x10 | ((v)&0xF))
#define SET_TONE_LATCH(c, t)	(0x80 | ((c) << 5) | 0x00 | ((t)&0xF))
#define SET_TONE_DATA(c, t)		(0x00 | ((t) >> 4))
#define SET_NOISE(c, n)			(0 & (c) & (n)) //(0xE0 | ((c) << 5) | 0x00 | ((n) & 0xF))
                                //will result to 0, avoid 'var set but not used warning ;)

/************
 * .sn7 file format
 *
 * n frames:
 * dc.b     mixer   llllnccc    l = loop (how many update use the same frame) => coming soon
 *                              n = noise enabled
 *                              c = channels (1 bit per channel)
 *
 * !!!!!!!!!!! WARNING ccc isn't handled yet, so, for now, the 3 channels are defined each frame !!!!!!!!!!!!!!!
 *
 * dc.b		noise               if (mixer & 00001000b)
 * dc.b 	volume chan 0       if (mixer & 00000001b)
 * dc.b		tone high chan 0    if (mixer & 00000001b)
 * dc.b		tone low chan 0     if (mixer & 00000001b)
 * dc.b 	volume chan 1       if (mixer & 00000010b)
 * dc.b		tone high chan 1    if (mixer & 00000010b)
 * dc.b		tone low chan 1     if (mixer & 00000010b)
 * dc.b 	volume chan 2       if (mixer & 00000100b)
 * dc.b		tone high chan 2    if (mixer & 00000100b)
 * dc.b		tone low chan 2     if (mixer & 00000100b)
 *
 * values are ready to be send to PSG port (ie include channel, ...)
 * mixer = 00  means end of sound
 *
 *
 ***********/
struct sn76489_frame
{
	u8 mixer;
	u8 noise;
	struct channel{
        u8 volume;
        u8 toneLatch;
        u8 toneData;
	}channels[3];
};


struct options
{
	int rate;
	int destRate;
	u8 channels[3];
	u8 export;
	u8 startFrame;
	u8 maxFrame;
	u8 maxVolume;
	u8 noLoop;
	u8 noNoise;
} psg_options;
static char exportFilename[MAX_PATH];


static u8 frameCopy( struct sn76489_frame *frame, u8 *dest)
{
    //TODO don't write a full frame but part of it as needed, according frame->mixer
    memcpy(dest, frame, sizeof(struct sn76489_frame));
    return sizeof(struct sn76489_frame);
}
static u8 flushToFile(FILE *output, unsigned char *buffer, long size, char *filename)
{
	char string[MAX_PATH];
	FILE *outfile;
	u8 endMarker;

	outfile = fopen(filename, "wb");
	if (outfile == NULL)
	{
		printf("Can't create %s\n", filename);
		return FALSE;
	}

	if ( fwrite(buffer,size, 1, outfile) != 1)
	{
		printf("Can't write to file %s\n", filename);
		fclose(outfile);
		return FALSE;
	}

    endMarker = 0;
	fwrite(&endMarker,sizeof(endMarker), 1, outfile);

	fclose(outfile);

	sprintf(string, "\tINCBIN \"%s\"\n", filename);
	fwrite(string, strlen(string), 1, output);

	return TRUE;
}

static void printToOutput(FILE *output, u8 *buffer, long size)
{
	char string[MAX_PATH];
	u8 *ptr;
	long i;
	u8 chan;
	struct sn76489_frame frame;

	ptr = buffer;
	for (i=0; i< size; i+= sizeof(struct sn76489_frame))
	{
		memcpy(&frame, ptr, sizeof(struct sn76489_frame));
		ptr+= sizeof(struct sn76489_frame);

        sprintf(string, "\tdc.b 0x%.2X\t;mixer\n", frame.mixer);
		fwrite(string, strlen(string), 1, output);

		if (frame.noise)
			sprintf(string, "\tdc.b 0x%.2X\t;set noise (%s)\n", frame.noise, (frame.noise & 0x4)?"white":"periodic");
		else
			sprintf(string, "\tdc.b 0\t\t;no noise\n");
		fwrite(string, strlen(string), 1, output);

		for (chan = 0; chan < 3; chan ++)
        {
            if ((frame.channels[chan].volume & 0xF) == 0xF)
                sprintf(string, "\tdc.b 0x%.2X\t;disabled chan %d\n", frame.channels[chan].volume, chan);
            else
                sprintf(string, "\tdc.b 0x%.2X\t;set volume chan %d\n", frame.channels[chan].volume, chan);
            fwrite(string, strlen(string), 1, output);

            if ( frame.channels[chan].toneLatch || frame.channels[chan].toneData ) //TODO use mixer to define is used or not
                sprintf(string, "\tdc.w 0x%.2X%.2X\t;set tone chan %d\n", frame.channels[chan].toneLatch, frame.channels[chan].toneData, chan);
            else
                sprintf(string, "\tdc.w 0\t\t;no tone chan %d\n", chan);
            fwrite(string, strlen(string), 1, output);
        }
        sprintf(string, "\n");
        fwrite(string, strlen(string), 1, output);
	}

	sprintf(string, "\tdc.b 0\t\t;end\n");
	fwrite(string, strlen(string), 1, output);
}

static u8 getVolume(u8 ayVolume)
{
    //TODO : convert to sn attenuator ?
	u8 newVal = (0xF - (ayVolume&0xF));//skipping the bit4 (amplitude mode)
	if (newVal < psg_options.maxVolume)	newVal = psg_options.maxVolume;
    return newVal;
}
static u8 getNoise( u8 ayNoise)
{
	u16 snNoise = 0;

	//TODO

	return snNoise;
}


static u16 getTone( u16 ayTone)
{
	u16 snTone;
	if (ayTone == 0)
	{
		snTone = 0;
	}
	else
	{
		double ayFreq, snFreq;

		ayFreq = psg_options.rate/(16.0*ayTone);

		//according source rate, bounds could be exceeded so, in this case....
		if ( ayFreq < (psg_options.destRate/(32.0*0x3FF)) )
		{
			//pitch too high
			snFreq = psg_options.destRate/(32.0*0x3FF);
			snTone = 0x3FF;

			if (verbose)
			{
				printf("Pitch too high, adapt %f to %f\n", ayFreq, snFreq);
			}
		}
		else if ( ayFreq > (psg_options.destRate/(32.0)) )
		{
			//pitch too low
			snFreq = psg_options.destRate/(32.0);
			snTone = 1;

			if (verbose)
			{
				printf("Pitch too low, adapt %f to %f\n", ayFreq, snFreq);
			}
		}
		else
		{
			//regular way to convert AY to SN freq : find the tone equivalent for a given frequency
			//psg HZ = PSGMasterClock/32*psgTone
			//ay HZ = AYMasterClock/16*ayTone
			snTone = psg_options.destRate/ (32.0*ayFreq);
			snFreq = psg_options.destRate / (32.0*snTone);
		}
#ifdef DEBUG
		if (verbose)
		{
		printf("Convert tone %.4x to %.4x\n", ayTone, snTone);
		printf("Convert tone %f to %f\n", ayFreq, snFreq);
		}
#endif

	}

	return snTone;
}



static u32 getCRC( struct sn76489_frame *frame)
{
    u32 crc = 0;
    u8 chan;
    crc = frame->mixer;
    crc += frame->noise;

    for( chan =0; chan < 3; chan++)
    {
        crc += frame->channels[chan].volume;
        crc += frame->channels[chan].toneLatch;
        crc += frame->channels[chan].toneData;
    }

    return crc;
}


static u32 handleYM( char *filename, u8 **dest)
{
	u8 *buffer;
	u8 currentMappedChan;
	u32 convertSize = 0;
	u32 previousCRC = 0;
	YMMUSIC *ym;
	ymMusicInfo_t ymInfo;
	ymChipInfo_t ymChipInfo;

	int ymFrame = 0;
	struct sn76489_frame frame;

	ym = ymMusicCreate();
	if (!ym)
	{
		printf("Internal error : YM engine unable to init\n");
		return convertSize;
	}
	if (!ymMusicLoad(ym, (const char *) filename) )
	{
		ymMusicDestroy(ym);
		printf("Invalid YM file %s\n", filename);
		return convertSize;
	}


	ymMusicGetInfo(ym, &ymInfo);
	ymMusicGetChipInfo(ym, &ymChipInfo);
	if (verbose)
	{
		printf("\n  YM file loaded:\n\tSong name\t: %s\n", strlen(ymInfo.pSongName)?ymInfo.pSongName:"- none -");
		printf("\tSong author\t: %s\n", strlen(ymInfo.pSongAuthor)?ymInfo.pSongAuthor:"- none -");
		printf("\tSong comment\t: %s\n", strlen(ymInfo.pSongComment)?ymInfo.pSongComment:"- none -");
		//printf("\tSong player\t: %s\n", ymInfo.pSongPlayer);
		printf("\tSong type\t: %s\n", ymInfo.pSongType);
		printf("\tFrame count\t: %d\n", ymChipInfo.nbFrame);
		printf("\tFrame size\t: %d bytes\n", ymChipInfo.frameSize);
		printf("\tSong time\t: %ld ms\n", (long) ymInfo.musicTimeInMs);
		printf("\tSource clock\t: %ld Hz\n", (long) ymChipInfo.clock);
	}

    if (psg_options.rate == SOURCE_AUTO)
        psg_options.rate = ymChipInfo.clock;

	ymMusicSetLoopMode(ym,YMFALSE);			// to avoid never ending conversion

#if DEBUG
	if (verbose)
	{
		printf("AY freq range = [%f,%f]\n", psg_options.rate/16.0, psg_options.rate/(16.0*0xFFF));
	    printf("SN freq range = [%f,%f]\n", psg_options.destRate/32.0, psg_options.destRate/(32.0*0x3FF));
	}
#endif // DEBUG

    //alloc 1 sn frame per ay frame, zero'ed
	*dest = (u8 *) calloc( ymChipInfo.nbFrame,  sizeof(struct sn76489_frame) );
	if (*dest == NULL)
	{
		ymMusicDestroy(ym);
		printf("Can't allocate %d bytes\n",sizeof(struct sn76489_frame));
		return 0;
	}
	buffer = *dest;

	//TODO handle != 16 frameSize ?
	for (ymFrame = 0; ymFrame < ymChipInfo.nbFrame; ymFrame++)
	{
	    /********************************
	    YM5/6 frame is like AY-3-8910 registers

	    0              Channel A fine pitch            8-bit (0-255)
        1              Channel A course pitch          4-bit (0-15)
        2              Channel B fine pitch            8-bit (0-255)
        3              Channel B course pitch          4-bit (0-15)
        4              Channel C fine pitch            8-bit (0-255)
        5              Channel C course pitch          4-bit (0-15)
        6              Noise pitch                     5-bit (0-31)
        7              Mixer                           8-bit (see below)
        8              Channel A volume                4-bit (0-15, see below)
        9              Channel B volume                4-bit (0-15, see below)
        10             Channel C volume                4-bit (0-15, see below)
        11             Envelope fine duration          8-bit (0-255)
        12             Envelope course duration        8-bit (0-255)
        13             Envelope shape                  4-bit (0-15)
        14             I/O port A                      8-bit (0-255)
        15             I/O port B                      8-bit (0-255)


        Frequency
        source rate / 16 / 12bits pitch

        Noise
        //TODO

        Mixer (low=enabled)
        Bit: 7        6        5        4        3        2        1        0
            _         _
            I/O       I/O   Noise    Noise    Noise     Tone     Tone     Tone
             B         A        C        B        A        C        B        A
        The AY-3-8912 ignores bit 7 of this register.

        Volume
        4-bit setting, with 0 is silence
        if bit 5 is set then that channel uses the envelope defined by register 13 and ignores its volume setting.

        Enveloppe shape
         0      \__________     single decay then off

         4      /|_________     single attack then off

         8      \|\|\|\|\|\     repeated decay

         9      \__________     single decay then off

        10      \/\/\/\/\/\     repeated decay-attack
                  _________
        11      \|              single decay then hold

        12      /|/|/|/|/|/     repeated attack
                 __________
        13      /               single attack then hold

        14      /\/\/\/\/\/     repeated attack-decay

        15      /|_________     single attack then off

	    *********************************/
		u16 ayTone = 0, snTone = 0;
		u8 ayNoise = 0, snNoise = 0;
		u8 mixer = 0;
		u8 volume = 0;
		u32 newCRC;


        if (ymFrame < psg_options.startFrame)   continue;   //skip frame

        if (psg_options.maxFrame > 0)
        {
            if (ymFrame >= (psg_options.startFrame+psg_options.maxFrame))   break;   //max frame reached
        }

		const ymu8 *data = ymGetFrame(ym, ymFrame);

        memset(&frame, 0, sizeof(frame));

        //mixer |enabled (ie 0 means enabled)
		mixer = data[7];
        //frame.mixer = 0x10 | (~mixer & 0x7);
        frame.mixer = 0x10; //at least one loop
        if ((~mixer & 0x1) && (psg_options.channels[0] != CHAN_DISABLED))
            frame.mixer |= (1 << psg_options.channels[0]);
        if ((~mixer & 0x2) && (psg_options.channels[1] != CHAN_DISABLED))
            frame.mixer |= (1 << psg_options.channels[1]);
        if ((~mixer & 0x4) && (psg_options.channels[2] != CHAN_DISABLED))
            frame.mixer |= (1 << psg_options.channels[2]);



        // noise
		/*
			if ( !GET_BIT(mixer,3) )
			{
				//noise enabled chan 0
			}

			if ( !GET_BIT(mixer,4) )
			{
				//noise enabled for chan 1
			}

			if ( !GET_BIT(mixer,5) )
			{
					  //noise enabled for chan 2
			}
		*/
        if ( ((mixer & 0x38) != 0x38 ) && (psg_options.noNoise == FALSE))
        {
            //TODO
            //if noise enable for at least on one chan...
            //SET_BIT(frame.mixer, MIXER_BIT_NOISE);
        }

		ayNoise = data[6] & 0x1F;
		snNoise = getNoise(ayNoise);
		frame.noise = SET_NOISE(0, snNoise);
		if (psg_options.noNoise) frame.noise = 0;


		//chan A
		currentMappedChan = psg_options.channels[0];
		if ( (GET_BIT(mixer,0) == 0) && (currentMappedChan != CHAN_DISABLED) )
		{
            ayTone = data[1];
            ayTone <<= 8;
            ayTone |= data[0];
            volume = getVolume( data[8] );

            snTone = getTone(ayTone);
            frame.channels[ currentMappedChan ].volume = SET_VOLUME(currentMappedChan, volume);
            frame.channels[ currentMappedChan ].toneLatch = SET_TONE_LATCH(currentMappedChan, snTone);
            frame.channels[ currentMappedChan ].toneData = SET_TONE_DATA(currentMappedChan, snTone);
		}

		//chan B
		currentMappedChan = psg_options.channels[1];
		if ( (GET_BIT(mixer,1) == 0) && (currentMappedChan != CHAN_DISABLED) )
        {
            ayTone = data[3];
            ayTone <<= 8;
            ayTone |= data[2];
            volume = 0xF - (data[9] & 0xF); //skipping the bit4 (amplitude mode)

            snTone = getTone(ayTone);
            frame.channels[ currentMappedChan ].volume = SET_VOLUME(currentMappedChan, volume);
            frame.channels[ currentMappedChan ].toneLatch = SET_TONE_LATCH(currentMappedChan, snTone);
            frame.channels[ currentMappedChan ].toneData = SET_TONE_DATA(currentMappedChan, snTone);
        }

		//chan C
		currentMappedChan = psg_options.channels[2];
		if ( (GET_BIT(mixer,2) == 0) && (currentMappedChan != CHAN_DISABLED) )
        {
            ayTone = data[5];
            ayTone <<= 8;
            ayTone |= data[4];
            volume = 0xF - (data[10] & 0xF); //skipping the bit4 (amplitude mode)

            snTone = getTone(ayTone);
            frame.channels[ currentMappedChan ].volume = SET_VOLUME(currentMappedChan, volume);
            frame.channels[ currentMappedChan ].toneLatch = SET_TONE_LATCH(currentMappedChan, snTone);
            frame.channels[ currentMappedChan ].toneData = SET_TONE_DATA(currentMappedChan, snTone);
        }

        //detect loop
        newCRC = getCRC(&frame);
        if ( newCRC == previousCRC )
        {
            u8 prevMixer = *buffer;
            prevMixer >>= 4;
            if ( prevMixer < 0xF)
            {
                prevMixer++;
                frame.mixer = ( (prevMixer<<4) | (frame.mixer&0xF) );
                frameCopy(&frame, buffer); //overwrite full to only change mixer..yes, i know ;)
                continue;
            }
        }
        if (previousCRC != 0)
            buffer+=sizeof(frame);

		convertSize+= frameCopy(&frame, buffer);
        previousCRC = newCRC;

        if (psg_options.noLoop) previousCRC = 1; //will force the buffer+=sizeof(frame) line
	}

	ymMusicDestroy(ym);

    return (convertSize);
}

static u32 handleYM3( char *filename, u8 **dest)
{
	return handleYM(filename, dest);
}

static u32 handleYM5( char *filename, u8 **dest)
{
	return handleYM(filename, dest);
}

static u32 handleYM6( char *filename, u8 **dest)
{
	return handleYM(filename, dest);
}

static u32 handleAFX( char *filename, u8 **dest)
{
	long fileSize;
	u16 frameIdx = 0;
	u32 convertSize = 0;
	u8 *source;
	u8 *ptr;
	u8 *buffer;

	//PSG channel used to play this SFX
	//defaut is 0 but perhaps it was remapped
	u8 destChan = psg_options.channels[0];

	u8 flagByte;
	u8 volume = 0;
	u16 ayTone = 0, snTone = 0;
	u8 ayNoise = 0, snNoise = 0;

	struct sn76489_frame frame;
	FILE *infile;

    if (destChan == CHAN_DISABLED)
    {
		printf("Error : AFX uses chan 0, remap it but don't disable it\n");
		return FALSE;
    }

	fileSize = file_getSize(filename);

	infile = fopen(filename, "rb");
	if (infile == NULL)
	{
		printf("Can't open %s\n", filename);
		return FALSE;
	}

	source = (unsigned char *)malloc(fileSize);
	memset(source, 0, fileSize);

	fread(source, 1, fileSize, infile);
	fclose(infile);


	ptr = source;

    //we start with a frame, since we don't know how much we need right now
	*dest = (u8 *) malloc(sizeof(struct sn76489_frame));
	if (*dest == NULL)
	{
		printf("Can't allocate %d bytes\n",sizeof(struct sn76489_frame));

		if (source)		free(source);
		return 0;
	}
	buffer = *dest;

#if DEBUG
    if (verbose)
    {
        printf("AY freq range = [%f,%f]\n", psg_options.rate/16.0, psg_options.rate/(16.0*0xFFF));
        printf("SN freq range = [%f,%f]\n", psg_options.destRate/32.0, psg_options.destRate/(32.0*0x3FF));
    }
#endif // DEBUG

	flagByte = *ptr;
	ptr++;

    //reset it only one, since we use previous data each new frame
    memset(&frame, 0, sizeof(frame));
    frame.mixer = 0x10 | (1 << destChan);  //1 loop, chan X only, w/o noise


	//end sequence : 0xD0 0x20
	while ( !((flagByte == 0xD0) && (*ptr == 0x20)) )
	{
	    //important : keep snTone and snNoise for each loop since they are used when enabled

        if ( psg_options.maxFrame != 0)
        {
            if (frameIdx >= (psg_options.startFrame+psg_options.maxFrame))   break;   //max frame reached
        }


		if (convertSize > 0)
		{
		    //we need another frame
			*dest = (u8 *) realloc(*dest, convertSize+sizeof(struct sn76489_frame));
			if (*dest == NULL)
			{
				printf("Can't allocate %ld bytes\n",convertSize+sizeof(struct sn76489_frame));

				if (source)		free(source);
				return 0;
			}
		}
		buffer = *dest;
		buffer += convertSize;

		/*
		Every AFX frame is encoded with the flag byte and a number of bytes that vary depending from value changes.
			bit0..3  Volume
			bit4     Disable T
			bit5     Change Tone
			bit6     Change Noise
			bit7     Disable N
		when bit5 is set, two bytes with tone period are follow
		when bit6 is set, a single byte with noise period follow
		when both bits are set, first two bytes of tone period, then single byte with noise period follow.
		when none of the bits are set, next flags byte follow.

		a frame is 1/50

		Volume is 0-F (with 0 silent, invert of SN)
		Tone is 0000 - 0FFF
		Noise is 00-1F
		*/

		volume = getVolume((flagByte & AY_BIT_VOLUME_VALUE));
		frame.channels[destChan].volume = SET_VOLUME(destChan, volume);

#if DEBUG
		if (verbose)
		{
			printf("Set volume : 0x%.2X\n", volume);
		}
#endif

		//IMPORTANT : handle tone BEFORE noise since noise byte follow option tone bytes
		if (flagByte & AY_BIT_TONE_CHANGE)
		{
			ayTone = *ptr;
			ptr++;
			ayTone |= ( *ptr << 8 );
			ptr++;

			snTone = getTone( ayTone );

		}

		if (flagByte & AY_BIT_TONE_DISABLE)
		{
#if DEBUG
			if (verbose)
			{
				printf("Disable tone : unsupported\n");
			}
#endif
            CLEAR_BIT(frame.mixer, 0);

            frame.channels[destChan].toneLatch = 0;
			frame.channels[destChan].toneData = 0;
		}
		else
		{
#if DEBUG
			if (verbose)
			{
				printf("Set tone :  0x%.4X => 0x%.4X\n", ayTone, snTone);
			}
#endif
            SET_BIT(frame.mixer, 0);
			frame.channels[destChan].toneLatch = SET_TONE_LATCH(destChan, snTone);
			frame.channels[destChan].toneData = SET_TONE_DATA(destChan, snTone);
		}



		if (flagByte & AY_BIT_NOISE_CHANGE)
		{
			ayNoise = *ptr;
			snNoise = getNoise(ayNoise);
			ptr++;
		}

		if ( (flagByte & AY_BIT_NOISE_DISABLE) || psg_options.noNoise )
		{
			CLEAR_BIT(frame.mixer, MIXER_BIT_NOISE);
			frame.noise = 0;
		}
		else
		{
		    //TODO
            //SET_BIT(frame.mixer, MIXER_BIT_NOISE);
            frame.noise = snNoise;
		}

        if (frameIdx >= psg_options.startFrame)
            convertSize+=frameCopy(&frame, buffer);
        //else don't write to buffer

		flagByte = *ptr;
		ptr++;
		frameIdx++;
	}

	if (source)		free(source);

	return convertSize;
}

static u8 processFile( FILE *output, char *id, char *filename  )
{
	long	convertSize;

	u8 *dest;
	char string[MAX_PATH];


	if (!file_isValid(filename))
	{
		printf("File %s doesn't exist\n", filename);
		return FALSE;
	}


	convertSize = 0;
	if ( file_isFileType(filename, AFX_TYPE) )
	{
		convertSize = handleAFX( filename, &dest );
	}
	else if ( file_isFileType(filename, YM3_TYPE) )
    {
        convertSize = handleYM3( filename, &dest );

    }
	else if ( file_isFileType(filename, YM5_TYPE) )
    {
		convertSize = handleYM5( filename, &dest );

    }
	else if ( file_isFileType(filename, YM6_TYPE) )
    {
		convertSize = handleYM6( filename, &dest );
    }
	else if ( file_isFileType(filename, YM_TYPE) )
	{
		convertSize = handleYM( filename, &dest );
	}
    else
    {

        printf("Unsupported file type\n");

        return FALSE;
    }



	if (convertSize == 0)
	{
		printf("Error importing file : %s\n", filename);
		return FALSE;
	}

	sprintf(string, ";;;;;;;;;; PSG\n\t.align 2\n\t.globl %s\n%s:\n", id, id);
	fwrite(string, strlen(string), 1, output);
	if (psg_options.export)
	{
		char outputFilename[MAX_PATH];
		file_getNewFilename(filename, outputFilename, ".sn7");

		if ( flushToFile(output, dest, convertSize, outputFilename) == FALSE)
		{
			if (dest)		free(dest);
			return FALSE;
		}
	}
	else
	{
		printToOutput(output, dest, convertSize);
	}
	sprintf(string, "\n\t.align 2\n\t.globl %s_duration\n%s_duration:\n\tdc.l %ld\n\n", id, id, convertSize);
	fwrite(string, strlen(string), 1, output);

	if (dest)		free(dest);

	return TRUE;
}


static void handleRemap(u8 chan, char *text)
{
	char *option;
	char *ptr = text;
	unsigned int newChan;

	trim(ptr, EMPTY_CHAR);

	option = strstr(ptr, "DISABLED");
	if (option != NULL)
	{
		psg_options.channels[chan] = CHAN_DISABLED;
		return;
	}

	if ( sscanf(ptr, "%u", &newChan) == 1 )
	{
		if ( /*(newChan>=0) &&*/ (newChan<=2) )
			psg_options.channels[chan] = newChan;
		else
		    printf("Invalid TONE_%d value : %d\n\tValid values are DISABLED, 0, 1 or 2\n", chan, newChan);
		return;
	}

    printf("Invalid TONE_%d value : %s\n", chan, ptr);
}

static void handleNoNoise(char *text)
{
    psg_options.noNoise = TRUE;
}

static void handleNoLoop(char *text)
{
    psg_options.noLoop = TRUE;
}

static void handleMaxVolume(char *text)
{
	char *ptr = text;
	int value;

	trim(ptr, EMPTY_CHAR);

	if ( sscanf(ptr, "%d", &value) == 1 )
	{
		if ((value >=0 ) && (value < 0xF))
		{
			psg_options.maxVolume = value;
		}
		else
		{
		    printf("Wrong MAX_VOLUME value: %s (should be between 0 and 0xE)\n", ptr);
		}
	}
	else
    {
        printf("Wrong MAX_VOLUME value: %s\n", ptr);
    }
}

static void handleMaxFrame(char *text)
{
	char *ptr = text;
	int value;

	trim(ptr, EMPTY_CHAR);

	if ( sscanf(ptr, "%d", &value) == 1 )
	{
		psg_options.maxFrame = value;
	}
	else
    {
        printf("Wrong MAX_FRAME value: %s\n", ptr);
    }
}

static void handleStartFrame(char *text)
{
	char *ptr = text;
	int value;

	trim(ptr, EMPTY_CHAR);

	if ( sscanf(ptr, "%d", &value) == 1 )
	{
		psg_options.startFrame = value;
	}
	else
    {
        printf("Wrong START_FRAME value: %s\n", ptr);
    }
}
static void handleExport(char *text)
{
	char *ptr = text;

	psg_options.export = TRUE;

	trim(ptr, EMPTY_CHAR);
	if ( strlen(ptr) == 0 )
	{
	    //no filename
	    exportFilename[0] = '\0';
	    return;
	}

	if ( sscanf(ptr, "%s", exportFilename) < 1 )
	{
	    exportFilename[0] = '\0';
	}
	//else we got a filename!
}
static void handleDestRate(char *text)
{
	char *option;
	char *ptr = text;
	int freq;

	trim(ptr, EMPTY_CHAR);

	if ( sscanf(ptr, "%d", &freq) == 1 )
	{
		psg_options.destRate = freq;
		return;
	}

	//else try helpers (DEST_xxx)
    option = strstr(ptr, "DEST_PAL");
    if (option != NULL)
    {
        psg_options.destRate = DEST_PAL;
        return;
    }

    option = strstr(ptr, "DEST_NTSC");
    if (option != NULL)
    {
        psg_options.destRate = DEST_NTSC;
        return;
    }

    printf("Invalid DESTINATION value : %s\n", ptr);
}

static void handleSourceRate(char *text)
{
	char *option;
	char *ptr = text;
	int freq;

	trim(ptr, EMPTY_CHAR);

	if ( sscanf(ptr, "%d", &freq) == 1 )
	{
		psg_options.rate = freq;
		return;
	}

	//else try helpers (SOURCE_xxx)
    option = strstr(ptr, "SOURCE_AUTO");
    if (option != NULL)
    {
        psg_options.rate = SOURCE_AUTO;
        return;
    }

    option = strstr(ptr, "SOURCE_AYFX");
    if (option != NULL)
    {
        psg_options.rate = SOURCE_AYFX;
        return;
    }
    option = strstr(ptr, "SOURCE_AMSTRAD");
    if (option != NULL)
    {
        psg_options.rate = SOURCE_AMSTRAD;
        return;
    }
    option = strstr(ptr, "SOURCE_ATARI");
    if (option != NULL)
    {
        psg_options.rate = SOURCE_ATARI;
        return;
    }
    option = strstr(ptr, "SOURCE_INTELLIVISION");
    if (option != NULL)
    {
        psg_options.rate = SOURCE_INTELLIVISION;
        return;
    }
    option = strstr(ptr, "SOURCE_MSX");
    if (option != NULL)
    {
        psg_options.rate = SOURCE_MSX;
        return;
    }
    option = strstr(ptr, "SOURCE_SPECCY");
    if (option != NULL)
    {
        psg_options.rate = SOURCE_SPECCY;
        return;
    }
    option = strstr(ptr, "SOURCE_VECTREX");
    if (option != NULL)
    {
        psg_options.rate = SOURCE_VECTREX;
        return;
    }

    printf("Invalid SOURCE value : %s\n", ptr);

}

static void handleOption(char *text)
{
    char *ptr = malloc( strlen(text)+1 );
	char *savePtr = ptr;

    memset(ptr, 0, strlen(text)+1);
    memcpy(ptr, text, strlen(text));


	if ( strncmp(ptr, "SOURCE" , strlen("SOURCE")) == 0 )
	{
		ptr += strlen("SOURCE");
		handleSourceRate(ptr);
	}
	else if(strncmp(ptr, "DESTINATION", strlen("DESTINATION")) == 0)
	{
		ptr += strlen("DESTINATION");
		handleDestRate(ptr);
	}
	else if(strncmp(ptr, "EXPORT", strlen("EXPORT")) == 0)
	{
		ptr += strlen("EXPORT");
		handleExport(ptr);
	}
    else if(strncmp(ptr, "START_FRAME", strlen("START_FRAME")) == 0)
	{
		ptr += strlen("START_FRAME");
		handleStartFrame(ptr);
    }
    else if(strncmp(ptr, "MAX_FRAME", strlen("MAX_FRAME")) == 0)
	{
		ptr += strlen("MAX_FRAME");
		handleMaxFrame(ptr);
	}
    else if(strncmp(ptr, "MAX_VOLUME", strlen("MAX_VOLUME")) == 0)
	{
		ptr += strlen("MAX_VOLUME");
		handleMaxVolume(ptr);
	}
	else if(strncmp(ptr, "NO_LOOP", strlen("NO_LOOP")) == 0)
	{
		ptr += strlen("NO_LOOP");
		handleNoLoop(ptr);
	}
	else if(strncmp(ptr, "NO_NOISE", strlen("NO_NOISE")) == 0)
	{
		ptr += strlen("NO_NOISE");
		handleNoNoise(ptr);
	}
	else if(strncmp(ptr, "TONE_0", strlen("TONE_0")) == 0)
	{
		ptr += strlen("TONE_0");
		handleRemap(0, ptr);
	}
	else if(strncmp(ptr, "TONE_1", strlen("TONE_1")) == 0)
	{
		ptr += strlen("TONE_1");
		handleRemap(1, ptr);
	}
	else if(strncmp(ptr, "TONE_2", strlen("TONE_2")) == 0)
	{
		ptr += strlen("TONE_2");
		handleRemap(2, ptr);
	}

	free(savePtr);
	return;
}
static u8 parseOptions(char *text)
{
	long i, nbLines;
	struct str_parserInfo *mappingParseInfo;

	mappingParseInfo = parser_load(text);
	if (mappingParseInfo == NULL)
	{
		if (verbose)	printf("%s",parser_error);
		return FALSE;
	}

    nbLines = parser_getLinesCount(mappingParseInfo);
	for (i = 0; i< nbLines; i++)
	{
		handleOption((char *)parser_getLine(mappingParseInfo, i));
	}

	parser_unload(mappingParseInfo);

	return TRUE;
}

////////////////////////////////////////////////
///////////
/////////// PUBLIC
///////////
////////////////////////////////////////////////
u8 psgExecute(char *info, FILE *output)
{
	char keyword[4]; //PSG\0
	char id[50];
	char file[MAX_PATH];
    char *options;
	int nbElem = 0;


	options = extractParameters(info);

	nbElem += sscanf(info, "%s", keyword);
	info += strlen(keyword);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "%s", id);
	info += strlen(id);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "\"%[^\"]\"", file);
	if (nbElem < 3)
	{
		printf("Wrong PSG definition\n");
		psgHelp();
		return FALSE;
	}
	info += (strlen(file) +2); 	//+2 for doublequote
	trim(info, EMPTY_CHAR);

    //init default
	psg_options.rate = SOURCE_AUTO;
	psg_options.destRate = DEST_NTSC;
	psg_options.export = FALSE;
	psg_options.maxFrame = 0;
	psg_options.maxVolume = 0;
	psg_options.startFrame = 0;
	psg_options.noLoop = FALSE;
	//TODO : enable noise
	psg_options.noNoise = TRUE; //FALSE;
	psg_options.channels[0] = 0;
	psg_options.channels[1] = 1;
	psg_options.channels[2] = 2;

    if (options != NULL)
		parseOptions(options);

    if (psg_options.rate == SOURCE_AUTO)
    {
	    if ( file_isFileType(file, AFX_TYPE) )
        {
			psg_options.rate = SOURCE_AYFX;
        }
	    //else if not a YM file
	    //psg_options.rate = SOURCE_AMSTRAD;
	}



	if (verbose)
	{
		printf("\n%s found\n", keyword);	 //PSG
		printf("  id  \t: %s\n", id);
		printf("  file\t: %s\n", file);
		if (psg_options.rate != SOURCE_AUTO)
			printf("  rate (in)\t: %d Hz\n", psg_options.rate );
		else
			printf("  rate (in)\t: - defined by file -\n");

		printf("  rate (out)\t: %d Hz\n", psg_options.destRate );

		if (psg_options.export)
			printf("  output\t: %s\n", strlen(exportFilename)?exportFilename:"- same as source");
        if (psg_options.startFrame != 0)
            printf("  first frame\t: %d\n", psg_options.startFrame);
		if (psg_options.maxFrame)
            printf("  max frame\t: %d\n", psg_options.maxFrame);
		if (psg_options.maxVolume != 0)
			printf("  max volume\t: %d\n", psg_options.maxVolume);

		if (psg_options.noLoop)
		    printf("  disable loop optimization\n");
		if (psg_options.noNoise)
		    printf("  disable noise\n");

		if (psg_options.channels[0] != 0)
		{
			if (psg_options.channels[0] != CHAN_DISABLED)
				printf("  YM tone0 mapped to PSG %d\n", psg_options.channels[0]);
			else
				printf("  YM tone0 disabled");
		}
		if (psg_options.channels[1] != 1)
		{
			if (psg_options.channels[1] != CHAN_DISABLED)
				printf("  YM tone1 mapped to PSG %d\n", psg_options.channels[1]);
			else
				printf("  YM tone1 disabled");
		}
		if (psg_options.channels[2] != 2)
		{
			if (psg_options.channels[2] != CHAN_DISABLED)
				printf("  YM tone2 mapped to PSG %d\n", psg_options.channels[2]);
			else
				printf("  YM tone2 disabled");
		}
	}

	processFile(output, id, file);

	return TRUE;
}

void psgHelp()
{
	printf("PSG can\n");
	printf("- convert AYEdit sfx to SN 76489 sfx\n");
	printf("- convert YMx file to SN 76489 sfx\n");
	printf("(noise not supported)\n");
//	printf("- transform a basic syntax to PSG data\n");
	printf("\nBasic usage:\n");
	printf("\tPSG id \"file\"\n");
	printf("\t{\n");
	printf("\t\t<options>\n");
	printf("\t\t...\n");
	printf("\t}\n");

	printf("\nwhere\n");

	printf("  id\t\tresource name\n");
	printf("  file\t\t.afx or .ym file to convert\n");

	printf("\n  Options:\n");
	printf("  - SOURCE <source master clock frequency in hz or helper>\n");
	printf("  \t- SOURCE_AUTO (default)\n");
	printf("  \t- SOURCE_AMSTRAD\n");
	printf("  \t- SOURCE_ATARI\n");
	printf("  \t- SOURCE_INTELLIVISION\n");
	printf("  \t- SOURCE_MSX\n");
	printf("  \t- SOURCE_SPECCY\n");
	printf("  \t- SOURCE_VECTREX\n");
	printf("  - DESTINATION <destination clock frequency in hz or helper>\n");
	printf("  \t- DEST_NTSC (default)\n");
	printf("  \t- DEST_PAL\n");
	printf("  - EXPORT [filename]\texport .sn7 file to source folder or specified filename\n");
	printf("  - START_FRAME number\tskip first <number> frames\n");
	printf("  - MAX_FRAME number\texport up to <number> frames\n");
	printf("  - MAX_VOLUME number\tforce volume max to <number> (0 max - 0xE min)\n");
	printf("  - NO_LOOP\tdisable mixer's loop, 1 frame = 1 genresPSGFrame_t\n");
	printf("  - NO_NOISE\tdisable noise\n");
	printf("  - TONE_0 <DISABLED or sn chan number>\tdisable or remap source tone 0 to (0-2)\n");
	printf("  - TONE_1 <DISABLED or sn chan number>\tdisable or remap source tone 1 to (0-2)\n");
	printf("  - TONE_2 <DISABLED or sn chan number>\tdisable or remap source tone 2 to (0-2)\n");
	printf("\n");

	printf("\nExample:\n");
	printf("PSG sfx_shoot \"resource\\msx_shoot.afx\"\n");
	printf("or\n");
	printf("PSG sfx_shoot \"resource\\msx_shoot.afx\"\n");
	printf("{\n");
	printf("\tSOURCE\t\tSOURCE_MSX\n");
	printf("\tDESTINATION\tDEST_NTSC\n");
	printf("\tEXPORT\t\t\"resource\\msx_shoot.sn7\"\n");
	printf("\tSTART_FRAME\t0\n");
	printf("}\n");

}
