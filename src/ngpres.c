#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ngpres.h"

#include "utils/file_utils.h"
#include "utils/parser.h"


u8 verbose = FALSE;



static void handleLine( char *line, FILE *output)
{
	char *keyword;

	line = trim(line, EMPTY_CHAR);
	if (strncmp(line, ";", 1) == 0)	return; //skip comment

	keyword = (char *)malloc(strlen(line)+1);
	if (keyword == NULL)	return;

    memset(keyword, 0, strlen(line)+1);

	sscanf(line, "%s", keyword);
	if (strlen(keyword)==0)
    {
        free(keyword);
        return; //skip empty line
    }
	if (strcmp(keyword, "DATA") == 0)
		dataExecute(line, output);
	else if (strcmp(keyword, "BITMAP") == 0)
		bitmapExecute(line, output);
	else if (strcmp(keyword, "FONT") == 0)
		fontExecute(line, output);
	else  if (strcmp(keyword, "PAL") == 0)
		palExecute(line, output);
	else if (strcmp(keyword, "HIRES") == 0)
		hiresExecute(line, output);
	else if (strcmp(keyword, "MAP") == 0)
		mapExecute(line, output);
	/*else if (strcmp(keyword, "SPRITE") == 0)
		spriteExecute(line, output);*/
	else if (strcmp(keyword, "SPRITESHEET") == 0)
		spritesheetExecute(line, output);
	/*else if (strcmp(keyword, "CONTROLS") == 0)
		controlsExecute(line, output);
	else if (strcmp(keyword, "DAC") == 0)
		dacExecute(line, output);
	else if (strcmp(keyword, "PSG") == 0)
		psgExecute(line, output); */
	else if (strcmp(keyword, "MERGE") == 0)
		mergeExecute(line, output);
	else
		printf("Unknown %s\n", keyword);

	free(keyword);
}

static void printHelp( char *resourceType)
{
	char *keyword;

	keyword = trim(resourceType, EMPTY_CHAR);
	if (strlen(keyword)==0)
    {
        return;
    }

	if (strcmp(keyword, "DATA") == 0)
		dataHelp();
	else if (strcmp(keyword, "BITMAP") == 0)
		bitmapHelp();
	else if (strcmp(keyword, "FONT") == 0)
		fontHelp();
	else if (strcmp(keyword, "PAL") == 0)
		palHelp();
	else if (strcmp(keyword, "HIRES") == 0)
		hiresHelp();
	else if (strcmp(keyword, "SPRITESHEET") == 0)
		spritesheetHelp();
	else if (strcmp(keyword, "MAP") == 0)
		mapHelp();
	/*	
	else if (strcmp(keyword, "SPRITE") == 0)
		spriteHelp();
	else if (strcmp(keyword, "CONTROLS") == 0)
		controlsHelp();
	else if (strcmp(keyword, "DAC") == 0)
		dacHelp();
	else if (strcmp(keyword, "PSG") == 0)
		psgHelp();
	*/
	else if (strcmp(keyword, "MERGE") == 0)
		mergeHelp();
	else
		printf("Unknown resourceType %s\n", keyword);
}


int main(int argc, char *argv[])
{
    FILE *in;
    FILE *out;
    long fileSize;
	char	string[MAX_PATH];

    struct str_parserInfo *parserInfo;
    long lineCount, i;
	char *text;
	char *line;

    if (argc < 3)
	{
    	//"genres help" gives the same result as "genres" or "genres input.rc"
		printf("\nNGPRes - the resource compiler for NeoGeo Pocket (Color)\n");
		printf("version 0.1 - %s by KanedaFr\n", __DATE__);
		printf("Use\n  - FreeImage library v%s to convert bitmap\n  - SoX library v%s to convert audio\n", FreeImage_GetVersion(), sox_version());
		printf("Visit http://www.spritesmind.net from more retro dev related infos and tools\n");

		printf("\nUsage:\n");
		printf("\tngpres input.rc output.asm [-v]\n");
		printf("\tngpres help [resourceType]\n");

		printf("\nOptions:\n");
		printf("\t -v active verbose output\n");

		printf("\nSupported resourceType :\n");
		printf("\tDATA\n");
		printf("\tFONT\n");
		printf("\tBITMAP\n");
		printf("\tPAL\n");
		printf("\tHIRES\n");
		//printf("\tSPRITE\n");
		printf("\tSPRITESHEET\n");
		printf("\tMAP\n");
		//printf("\tCONTROLS\n");
		//printf("\tDAC\n");
		//printf("\tPSG\n");
		
		return 0;
	}


    if (strcmp(argv[1], "help") == 0)
    {
    	printHelp(argv[2]);
    	return 0;
    }



    verbose = ( (argc == 4) && (strcmp(argv[3], "-v") == 0) );
	if (verbose)
		printf("Verbose mode : ON\n");


    if (!file_isValid(argv[1]))
        return -1;

    fileSize = file_getSize(argv[1]);
    in = fopen(argv[1], "rt");


	parserInfo = parser_loadFromFile( in, fileSize );
	if (parserInfo == NULL)
	{
		fclose(in);
		if (verbose)	printf("%s\n",parser_error);
		return -1;
	}

	fclose(in);

    if (verbose)    printf("Parsing done\n");




    ////// write output
	out = fopen(argv[2], "wt");
	if (out == NULL)
	{
		printf("Can't create %s", argv[2]);
		parser_unload(parserInfo);
		return -1;
	}

	sprintf(string, "f_const section romdata large\n\n");
	fwrite(string, strlen(string), 1, out);

	FreeImage_Initialise(TRUE);

	lineCount = parser_getLinesCount(parserInfo);
	for(i=0; i< lineCount; i++)
	{
		line = (char *) parser_getLine(parserInfo, i);
		if (strlen(line) == 0)  continue;

		//alloc enough for every char + 1 per line
		text = (char *) malloc(fileSize + lineCount);
		if (text == NULL)
		{
			printf("Can't allocate %ld bytes", fileSize + lineCount);
			break;
		}

        memset(text, 0, fileSize + lineCount);

		sprintf(text,"%s", line);


		if (i < lineCount-2)
		{
			line = (char *) parser_getLine(parserInfo, i+1);
			if (strlen(line) == 0)  continue;

			if (line[0] == '{')
			{
				while( (line[0] != '}') && (i < lineCount-1))
				{
					i++;
					line = (char *) parser_getLine(parserInfo, i);
					sprintf(text,"%s\n%s", text, line);
				}
			}
		}

		handleLine( text, out );

		free(text);
	}

	FreeImage_DeInitialise();

	sprintf(string, "\n\nend\n");
	fwrite(string, strlen(string), 1, out);

	fclose(out);

	parser_unload(parserInfo);
	
    return 0;
}
