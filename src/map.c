#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "genres.h"
#include "utils/packer.h"
#include "utils/parser.h"
#include "utils/mapwin.h"
#include "utils/file_utils.h"

#define MODE_TILE	0
#define MODE_CHUNK	1

#define MAPPY_LAYER_COUNT	8

struct stillBlocksOptions
{
	u8 packer;
	u8 exportPal;
	s16 count;
	u16 propertiesMask;
};

struct animatedBlocksOptions
{
	u8 packer;
	s16 count;
};

struct layerOptions
{
	u8 export;
	u8 packer;
};

struct options
{
	u8 mapMode; //0: tile, 1: chunk

	struct stillBlocksOptions exportStillBlocks;
	struct animatedBlocksOptions exportAnimatedBlocks;
	struct layerOptions exportLayers[MAPPY_LAYER_COUNT];
} map_options;

static u8	nbLayers;
static u16	nbTilePerBlock;

void exportMap( char *id, FILE *output )
{
	short int	*chunksData = NULL;
	int	chunkIdx;
	int tileX, tileY;
	u16 blockToExport = 0;

	int	packedSize = 0;
	unsigned char	*packed = NULL;

	int	layeridx;

	int	j, x, y;
	char	string[256];
	
	blockToExport = 0;
	if (map_options.exportStillBlocks.count == 0) 
		blockToExport = numblockstr;
	else
		blockToExport = map_options.exportStillBlocks.count;

	if (blockToExport > 512)
	{
		printf("WARNING : this map use more than 512 tiles\nPlease load only tiles needed to avoid memory corruption.\n");
	}

	
	
	if (blockToExport) 
	{
		short int	chunkLine;
		long tileDataSize;
		

		
		// create tiles data, for all chunk
		tileDataSize = 8*sizeof(int)*nbTilePerBlock*blockToExport;
		chunksData = (short int *)	malloc(tileDataSize);
		memset(chunksData, 0, tileDataSize);


		if (chunksData == NULL)
		{
			printf("Can't allocate %ldbytes for tiles", tileDataSize);
			return;
		}
	
		tileX = tileY = 0;
		for (chunkIdx = 0; chunkIdx < blockToExport; chunkIdx++)
		{
			//add each tile one by one, from left to right and top to bottom
			for (tileY = 0; tileY < blockheight/8; tileY++)
			{
				for (tileX = 0; tileX < blockwidth/8; tileX++)
				{
					for (y=0; y<8; y++)
					{
						chunkLine=0;

						
						for (x=0; x < 4; x++)
						{
							chunkLine <<= 2;
							chunkLine |= mpy_getPixel(x + tileX*8,y + tileY*8,chunkIdx) & 0x3;
						}
						for (x=0; x < 4; x++)
						{
							chunkLine <<= 2;
							chunkLine |= mpy_getPixel(x + 4+ tileX*8,y + tileY*8,chunkIdx)  & 0x3;
						}

						chunksData[ chunkIdx*nbTilePerBlock*8 + 8*(tileX + tileY*blockwidth/8) + y ] = chunkLine;
					}
				}
			}
		}
	}

	sprintf(string, ";;;;;;;;;; MAP \n\talign 2\n\tpublic _%s\n_%s:\n", id, id);
    fwrite(string, strlen(string), 1, output);

	if (map_options.mapMode == MODE_TILE)
		sprintf(string, "\tdw\t%d, %d\t ; map width and height, in tiles\n", mapwidth*blockwidth/8, mapheight*blockheight/8);
	else
		sprintf(string, "\tdw\t%d, %d\t ; chunk x chunk\n", mapwidth, mapheight);
    fwrite(string, strlen(string), 1, output);

	if ( (map_options.exportStillBlocks.count != -1) && (map_options.exportStillBlocks.exportPal))
	{
		sprintf(string, "\tdl\t%s_pal\n", id);
	}
	else
	{
		sprintf(string, "\tdl\t0\t; NO_PAL\n");
	}
    fwrite(string, strlen(string), 1, output);

	
	if (blockToExport)
	{
		if (map_options.mapMode == MODE_TILE)
			sprintf(string, "\tdw\t%d\t ; nb tiles\n", blockToExport*nbTilePerBlock);
		else
			sprintf(string, "\tdw\t%d\t ; nb chunk\n", blockToExport);
	}
	else
	{
		sprintf(string, "\tdw\t0\t ; no still block\n");
	}
    fwrite(string, strlen(string), 1, output);

	
	if (blockToExport)
	{
		packedSize = 0;
		if(map_options.exportStillBlocks.packer == ALGO_RLE)
		{
			packedSize = nbTilePerBlock*blockToExport*32;
			packed = compressRLE( (unsigned char *) chunksData, &packedSize);
		}
		sprintf(string, "\tdw\t0x%x\t; compressed size\n", packedSize); //force word since I doubt we'll go up to 0x10000 :)
	}
	else
		sprintf(string, "\tdw\t0\t; compressed size\n");
    fwrite(string, strlen(string), 1, output);


	if (blockToExport)
	{
		sprintf(string, "\tdl\t%s_tiles\n", id);
	}
	else
	{
		sprintf(string, "\tdl\t0\t ; no still block\n");
	}
    fwrite(string, strlen(string), 1, output);


	sprintf(string, "\tdw\t%d\t ; nb layers\n",  MAPPY_LAYER_COUNT);
    fwrite(string, strlen(string), 1, output);
	sprintf(string, "\tdl\t%s_layers\n", id);
    fwrite(string, strlen(string), 1, output);


	//pal
	if ( (map_options.exportStillBlocks.count != -1) && (map_options.exportStillBlocks.exportPal))
	{
		sprintf(string, "\n\talign 1\n%s_pal:\n", id);
        fwrite(string, strlen(string), 1, output);

		if (cmappt)
		{
			for (j=0; j<4; j++)
			{
				sprintf(string, "\tdw 0x%x%x%x\n", (cmappt[j*3 + 2]>>5)<<1, (cmappt[j*3 + 1]>>5)<<1, (cmappt[j*3]>>5)<<1);
                fwrite(string, strlen(string), 1, output);
			}
		}
		else
		{
			printf("no pal found! Did you use 256colors bitmap ?\n");
		}
	}


	if (blockToExport) 
	{
		//tiles
		sprintf(string, "\n\talign 1\n%s_tiles:\n", id);
        fwrite(string, strlen(string), 1, output);

		switch( map_options.exportStillBlocks.packer )
		{
			case ALGO_RAW:
				for(chunkIdx=0;chunkIdx<nbTilePerBlock*blockToExport;chunkIdx++)
				{
					sprintf(string, "; tile %d\n", chunkIdx);
                    fwrite(string, strlen(string), 1, output);

					for( y=0; y<8; y++)
					{
						sprintf(string, "\tdw 0x%.4x\n", chunksData[chunkIdx*8 + y]);
                        fwrite(string, strlen(string), 1, output);
					}
					sprintf(string, "\n");
                    fwrite(string, strlen(string), 1, output);
				}
				break;
			case ALGO_RLE:
				if (verbose)
					printf( getCompressionResult(string, nbTilePerBlock*blockToExport*32,packedSize) );

				//export word per word
				for(j=0;j<packedSize;j+=2)
				{
					sprintf(string, "\n\tdw\t0x%.2x%.2x", packed[j], packed[j+1]);
                    fwrite(string, strlen(string), 1, output);
				}
				sprintf(string, "\n\n");
                fwrite(string, strlen(string), 1, output);
				break;
		}
	}

	if (chunksData != NULL){
		free(chunksData);
	}
	if (packed != NULL){
        free(packed);
	}

	//layers
	sprintf(string, "\n\talign 1\n%s_layers:\n", id);
    fwrite(string, strlen(string), 1, output);

	for (layeridx = 0; layeridx < MAPPY_LAYER_COUNT; layeridx++)
	{
		if (map_options.exportLayers[layeridx].export == TRUE )
			sprintf(string, "\tdl\t%s_layer%d\n", id, layeridx);
		else
			sprintf(string, "\tdl\t0\t; layer %d not requested or invalid\n", layeridx);
        fwrite(string, strlen(string), 1, output);
	}

	for (layeridx = 0; layeridx < MAPPY_LAYER_COUNT; layeridx++)
	{
		if (map_options.exportLayers[layeridx].export == FALSE )	continue;
		
		sprintf(string, "\n\talign 1\n%s_layer%d:\n", id, layeridx);
        fwrite(string, strlen(string), 1, output);

		for (y=0; y < mapheight; y++)
		{
			for (x=0; x < mapwidth; x++)
			{
				chunkIdx = mpy_getBlock(layeridx, x, y);
				
				if (map_options.mapMode == MODE_TILE)
				{
					for (j=0; j < nbTilePerBlock; j++)
					{
						sprintf(string, "\tdw\t0x%x\n", chunkIdx*nbTilePerBlock + j  );
						fwrite(string, strlen(string), 1, output);
					}
				}
				else
				{
					sprintf(string, "\tdw\t0x%x\n", chunkIdx  );
					fwrite(string, strlen(string), 1, output);
				}
			}
		}

	}
	
	//TODO animated block

}


static void handleStillBlockOptions(char *text)
{
	int items_read, nbChar;
	u16 nbBlock;

	trim(text, EMPTY_CHAR);

	//not option
	if (strlen(text) == 0)	return;

	items_read = sscanf(text, "%hu%n", &nbBlock, &nbChar);
	if (items_read == 1)
	{
		map_options.exportStillBlocks.count = nbBlock;
		text += nbChar;

		trim(text, EMPTY_CHAR);
	}
	else
	{
		//if STILLBLOCKS asked, export all by default
		map_options.exportStillBlocks.count = 0; 
	}

	if (verbose)
	{
		if (map_options.exportStillBlocks.count == 0)
		{
			printf("Export %d still blocks\n", numblockstr);
		}
		else
		{
			printf("Export %d of the %d still blocks available\n", map_options.exportStillBlocks.count-1, numblockstr);
		}
	}

	//no more option
	if (strlen(text) == 0)	return;

	if ( strstr(text, "NO_PAL") != NULL )
	{
		map_options.exportStillBlocks.exportPal = FALSE;
	}
/*	
	if ( strstr(text, "PACKER_RLE") != NULL )
	{
			map_options.exportStillBlocks.packer = ALGO_RLE;
	}
*/	
	
	//TODO : handle properties
}


static void handleAnimatedBlockOptions(char *text)
{
	int items_read, nbChar;
	u16 nbAnimBlock;


	trim(text, EMPTY_CHAR);

	//no option
	if (strlen(text) == 0)	return;
	
	items_read = sscanf(text, "%hu%n", &nbAnimBlock, &nbChar);
	if (items_read == 1)
	{
		map_options.exportAnimatedBlocks.count = nbAnimBlock;
		text += nbChar;

		trim(text, EMPTY_CHAR);
	}
	else
	{
		//if ANIMATEDBLOCKS asked, export all by default
		map_options.exportAnimatedBlocks.count = 0; 
	}

	if (verbose)
	{
		if (map_options.exportAnimatedBlocks.count == 0)
		{
			printf("Export all animated blocks\n");
		}
		else
		{
			printf("Export up to animated block ID %d\n", map_options.exportAnimatedBlocks.count-1);
		}
	}

	//no more option
	if (strlen(text) == 0)	return;

}

static void handleLayersOptions(char *text)
{
	//int items_read, nbChar;
	//u16 nbLayers;


	trim(text, EMPTY_CHAR);

	//no option
	if (strlen(text) == 0)	return;

	//TODO
}

static void handleLayerSoloOptions(char *text)
{
	int items_read, nbChar;
	u16 idxLayer = 0xFFFF;


	trim(text, EMPTY_CHAR);

	//no option
	if (strlen(text) == 0)	return;
	
	items_read = sscanf(text, "%hu%n", &idxLayer, &nbChar);
	if (items_read == 1)
	{
		if ( (idxLayer<0) || (idxLayer>=nbLayers) )
		{
			printf("Error : invalid layer ID %d (only 0-%d are valid)\n", idxLayer, nbLayers-1);
			return;
		}
		
		text += nbChar;

		trim(text, EMPTY_CHAR);
	}
	
	if (idxLayer == 0xFFFF)
	{
		printf("Error : invalid layer ID\n");
		return;
	}
	
	if (verbose)
	{
		printf("Export layer %d\n", idxLayer);
	}
	
	map_options.exportLayers[idxLayer].export = TRUE;		
	//TODO : packer
}

static void handleLine(char *text)
{
	char *ptr = malloc( strlen(text)+1 );
	char *savePtr = ptr;

    memset(ptr, 0, strlen(text)+1);
    memcpy(ptr, text, strlen(text));

    if(strncmp(ptr, "STILLBLOCKS", strlen("STILLBLOCKS")) == 0)
	{
		ptr += strlen("STILLBLOCKS");
		handleStillBlockOptions(ptr);
	}
	else if (strncmp(ptr, "ANIMATEDBLOCKS", strlen("ANIMATEDBLOCKS")) == 0)
	{
		ptr += strlen("ANIMATEDBLOCKS");
		handleAnimatedBlockOptions(ptr);
	}
	else if (strncmp(ptr, "LAYERS", strlen("LAYERS")) == 0)
	{
		ptr += strlen("LAYERS");
		handleLayersOptions(ptr);
	}
	else if (strncmp(ptr, "LAYER", strlen("LAYER")) == 0)
	{
		ptr += strlen("LAYER");
		handleLayerSoloOptions(ptr);
	}
	else
	{
		if (verbose)
		{
			printf("Unhandled line %s\n", ptr);
		}
		free(savePtr);
		return;
	}
    free(savePtr);
}


u8 parseParameters(char *text)
{
	long i, nbLines;

	struct str_parserInfo *paramParseInfo;
	paramParseInfo = parser_load(text);
	if (paramParseInfo == NULL)
	{
		if (verbose)	printf("%s",parser_error);
		return FALSE;
	}

    nbLines = parser_getLinesCount(paramParseInfo);
	for (i = 0; i< nbLines; i++)
	{
		handleLine((char *)parser_getLine(paramParseInfo, i));
	}
	parser_unload(paramParseInfo);

	return TRUE;
}

u8 mappy_init(char *filename)
{
	strcpy( mapfilename, filename );
	if( LoadProject( ) != 0)
	{
		printf("File Error\n");
		return (FALSE);
	}
	
	

	if (blockdepth != 8)
	{
		printf("Invalid map file : not a 8bit map\n");
		return FALSE;
	}
	
	//check chunk is 8x size
	if (blockwidth%8 != 0)
	{
		printf("Invalid map file : blocks' width isn't multiple of 8 pixels\n");
		return FALSE;
	}
	if (blockheight%8 != 0)
	{
		printf("Invalid map file : blocks' height isn't multiple of 8 pixels\n");
		return FALSE;
	}
	nbTilePerBlock = (blockwidth/8) * (blockheight/8);

	//get max number fo layers defined on map file (they could be empty but we don't really care)
	for (nbLayers = 0; nbLayers<MAPPY_LAYER_COUNT; nbLayers++)
	{
		if (mapmappt[nbLayers] == NULL)	break;
	}

	
	return TRUE;
	
}

/////////// PUBLIC
u8 mapExecute(char *info, FILE *output)
{
	char keyword[4]; //MAP\0
	char id[50];
	char filename[256];
	char *option;
	int nbElem = 0;

	char *parameters;
	parameters = extractParameters(info);

	nbElem += sscanf(info, "%s", keyword);
	info += strlen(keyword);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "%s", id);
	info += strlen(id);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "\"%[^\"]\"", filename);
	if (nbElem < 3)
	{
		printf("Wrong MAP definition\n");
		mapHelp();
		return FALSE;
	}
	info += (strlen(filename) +2); 	//+2 for doublequote
	trim(info, EMPTY_CHAR);

	
	//default value is like  MAP1.0 : 1 layer, every tiles
	
	//tile mode
	map_options.mapMode   = MODE_TILE;

	// export all tiles, pal included but without properties
	map_options.exportStillBlocks.count= 0;
	map_options.exportStillBlocks.exportPal= TRUE;
	map_options.exportStillBlocks.packer= ALGO_RAW;
	map_options.exportStillBlocks.propertiesMask= 0;

	// don't export animated blocks
	map_options.exportAnimatedBlocks.count = -1;

	//export first layer only
	memset(&map_options.exportLayers, 0, sizeof(struct layerOptions)*MAPPY_LAYER_COUNT);
	map_options.exportLayers[0].export = TRUE;
	map_options.exportLayers[0].packer = ALGO_RAW;


	if ( strlen(info) )
	{
		option = strstr(info, "CHUNK");
		if (option != NULL)
		{
			map_options.mapMode = MODE_CHUNK;
		}
	}

	if (!file_isValid(filename))    return FALSE;

	if (verbose)
	{
		printf("\n%s found\n", keyword);
		printf("  id   : %s \n", id);
		printf("  file : %s \n", filename);
		printf("  mode : %s \n", (map_options.mapMode==MODE_CHUNK)?"CHUNK":"TILE");
	}

	if ( !mappy_init(filename) )
	{
		return FALSE;
	}
	
	if (parameters != NULL)
	{
		//if we use parameters, we're on MAP2.0, reset some default values inherited from MAP1.0
		map_options.exportLayers[0].export = FALSE;
		parseParameters(parameters);
	}

	exportMap( id, output );

	return (TRUE);
}

void mapHelp()
{
	printf("MAP extract data and tiles from a Mappy's FMP file\n");
	printf("Only 8bit tiles are supported\n");
	printf("Only the first 4 colors are imported\n");
	printf("Based on Mappy V1.3.11s by Robin Burrows\n");
	printf("Mappy is a free 2D map editor available at\n");
	printf("\t- http://www.tilemap.co.uk\n");
	printf("\t- http://www.geocities.com/SiliconValley/Vista/7336/robmpy.htm\n");
	printf("\nBasic usage:\n");
	printf("\tMAP id \"file\" [options]\n");
	printf("\t{\n");
	printf("\t\t<requested data>\n");
	printf("\t\t...\n");
	printf("\t}\n");

	printf("\nwhere\n");

	printf("  id\t\tresource name\n");
	printf("  file\t\tMappy's FMP file\n");

	printf("\n  Options:\n");
	printf("  - TILE\texport tile by tile (default)\n");
	printf("  - CHUNK\texport chunk by chunk\n");
	
	
	printf("\n  Requested data:\n");
	//TODO : properties
	printf("  - STILLBLOCKS max_block [pal option] [packing option]\n");
	printf("    	Max number of tile/chunk to export (0:all)\n");
	//experimental
	////printf("  - ANIMATEDBLOCKS max_block [pal option] [packing option]\n");
	////printf("    	Max number of tile/chunk to export (0:all)\n");
	//printf("  - LAYERS\n");
	//printf("    	TODO\n");
	//TODO : pack
	printf("  - LAYER index\n");
	printf("    	export layer <index> (0-%d)\n", MAPPY_LAYER_COUNT-1);

	printf("\n  Pal options:\n");
	printf("  - NO_PAL\tdoesn't export pal\n");
	
	printf("\n  Packing options:\n");
	printf("  - PACKER_NONE\tdoesn't pack data (default)\n");
//	printf("  - PACKER_RLE\tRLE packing\n");

	printf("\nExample:\n");

	printf("MAP level1 \"sonic_level1.map\" NO_PAL CHUNK PACKER_NONE\n");
	printf("{\n");
	printf("\tSTILLBLOCKS 0 PACKER_RLE\n");
	//experimental
	////printf("\tANIMATEDBLOCKS 0 NO_PAL\n");
	//TODO needed ?
	//printf("\tLAYERS\n");
	printf("\tLAYER 0\n");
	printf("\tLAYER 1\n");
	printf("}\n");
	
}
