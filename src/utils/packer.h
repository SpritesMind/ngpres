#ifndef __PACKER_H__
#define __PACKER_H__



#define ALGO_RAW	0
#define ALGO_RLE	1
//TODO : more ALGO_

#ifdef __cplusplus
extern "C" {
#endif

char *getCompressionName( char algo );
char *getCompressionResult( char *output, int rawSize, int packedSize );

unsigned char *compressRLE( unsigned char *src, int *srclen );


#ifdef __cplusplus
}
#endif

#endif
