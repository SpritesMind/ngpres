#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "packer.h"


char *getCompressionName(char algo)
{
	switch(algo)
	{
	case ALGO_RAW:
		return "none";
	case ALGO_RLE:
		return "RLE";
	}

	return "unknow";
}


char *getCompressionResult( char *result, int rawSize, int packedSize )
{
	sprintf(result, "Compression : %d%%%% (%dbytes / %dbytes)\n", packedSize*100/rawSize, packedSize, rawSize );
	return result;
}
/*************
 *
 *   Utility to compress and decompress VRAM data.
 *
 *   by Charles MacDonald
 *   WWW: http://cgfm2.emuviews.com
 *
 *
 *************/
unsigned char *compressRLE(unsigned char *src, int *srclen)
{
    unsigned char *nib;
    unsigned char *out;

    int outlen;
    int niblen;
    int i;

    unsigned char data;
    unsigned char rle;
    unsigned char in_run;

    niblen = *srclen * 2;

	//TODO : make out a parameter, to make it clear the out memory should be handled by caller
    /* Output data buffer (worst case is 1 byte for each nibble, 2x size) */
	//out = (UCHAR *)	HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY,niblen);
	out = (unsigned char *) malloc(niblen);
	if (out == NULL) return NULL;
	memset(out, 0, niblen);

    /* Expanded nibble buffer */
	//nib = (UCHAR *)	HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY,niblen);
	nib = (unsigned char *) malloc(niblen);
	if (nib == NULL)
	{
		//HeapFree(GetProcessHeap(), 0, out);
		free(out);
		return NULL;
	}
	memset(nib, 0, niblen);

    /* Unpack nibbles into bytes and swap dword ... we're in a x86 :(*/
    for(i = 0; i < *srclen; i+=4)
    {
        nib[i*2+0+0] = (src[i+3] >> 4) & 0x0F;
        nib[i*2+1+0] = (src[i+3] >> 0) & 0x0F;
        nib[i*2+0+2] = (src[i+2] >> 4) & 0x0F;
        nib[i*2+1+2] = (src[i+2] >> 0) & 0x0F;
        nib[i*2+0+4] = (src[i+1] >> 4) & 0x0F;
        nib[i*2+1+4] = (src[i+1] >> 0) & 0x0F;
        nib[i*2+0+6] = (src[i+0] >> 4) & 0x0F;
        nib[i*2+1+6] = (src[i+0] >> 0) & 0x0F;
    }

    in_run = 0;     /* Not in a run */
    outlen = 0;     /* No bytes written out */
    data = nib[0]+8;      /* Initial data value */

    for(i = 0; i < niblen; i++)
    {
        /* Are we encoding a run? */
        if(in_run)
        {
            /* Data has changed, write run and start new one */
            if(data != nib[i])
            {
                out[outlen++] = ((rle << 4) | data);
                data = nib[i];
                rle = 0;
            }
            else
            {
				++rle;
                /* Max run length reached, write run and stop */
                if( rle == 0x0f)
                {
                    out[outlen++] = (rle << 4 | data);
                    in_run = 0;
                }
            }
        }
        else
        {
            /* Start new run */
            in_run = 1;
            data = nib[i];
            rle = 0;
        }
    }

    /* If still in a run, write it out */
    if(in_run)
    {
        out[outlen++] = (rle << 4 | data);
    }

	//must be 2-aligned
	if (outlen%2 == 1)	outlen++;

    /* Assign new size and return output buffer */
    *srclen = outlen;

	//HeapFree(GetProcessHeap(), 0, nib);
    free(nib);

    return (out);
}
