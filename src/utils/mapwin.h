#ifndef __MAPWIN_H__
#define __MAPWIN_H__


#ifdef __cplusplus
extern "C" {
#endif

extern unsigned char trans16hi, trans16low, transred, transgreen, transblue, trans8bit;
extern short int sheight, swidth, sdepth, merror, mapwidth, mapheight, strtstr, strtblk, strtanim;
extern short int blockwidth, blockheight, blockdepth, blockstrsize, animspd, gridon, zoomon, maplayer;
extern short int xmapoffset, ymapoffset, span, numblockstr, numblockgfx, numblockanim, numbrushes;
extern short int curstr, curanim, oldxoff, oldyoff, oldblockx, oldblocky, curbrsh, revealanims, pastebrushnt;
extern long int bgcolour, fgcolour, curplacetile;
// new for V1.23 
extern int mapislsb, maptype;
extern int blockgapx, blockgapy, blockstaggerx, blockstaggery;
extern int clickmask, picklayer, picklx, pickly;
extern char * altgfxpt;
extern char novctext[70];
extern char mapfilename[256];
extern char gfxfilename[256];
extern short int * mapmappt[8];
extern unsigned char * cmappt;






/* mapfunc.c prototypes */

void FreeAll (void);
int CreateNewMap (int, int, int, int, int, int, int, int, int, int);
int LoadProject ( void );
int SaveProject (void);
short int * GetLayerpt (int);
int PickLayer (void);
int ChangeLayer (int);
int DeleteMapLayer (void);
int CreateMapLayer (void);
void Onionto24 (int, int, unsigned char *, int, int, int, int);
void Overlayto24 (int, int, unsigned char *, int, int, int, int);
void Blockto24 (int, int, unsigned char *, int, int, int);
void BlockSecto24 (int, unsigned char *, int, int, int, int);
void BlockGfxto24 (int ,unsigned char *, int ,int ,int);
void BlockGfxto8 (int ,unsigned char *, int ,int ,int);
void BlockStrto24 (int, unsigned char *, int, int, int);
void BlockAnimto24 (int, int, unsigned char *, int, int, int);
int IsBlockAttached (int);
void PlaceTile (int, int);
char * GetBrushName (int);
int MakeBrush (int, int, int, int);
int MakeBrushBS (int, int, int, int);
void DestroyAllBrushes (void);
void NewBlock (void);
void CutBlock (void);
void CopyBlock (void);
void PasteBlock (void);
void DestroyUnusedBlocks (void);
void CorrectBlockStructures (void);
void InitAnims (void);
void UpdateAnims (void);
void AnimNew (void);
void AnimCut (void);
void AnimCopy (void);
void AnimPaste (void);
void PickBlock (int, int);
void SetRefFrame (int, int);
void DelAnimFrame (int, int);
void DestroyUnusedAnims (void);
void InsertAnimFrame (int, int, int);
void DestroyUnusedGraphics (void);
int ResizeMap (int, int, int);
void MapFill (int, int, int);
void SetTranspColour (void);
void RemoveSingleGraphic (int);
void InsertSingleGraphic (int);

int mpy_getPixel ( int x, int y, int gnum );
int mpy_getBlock ( int n, int x, int y );

#ifdef __cplusplus
}
#endif

#endif
