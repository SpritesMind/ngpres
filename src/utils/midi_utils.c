//***************************************************************************
// "midi.c"
// MIDI-specific stuff, including parsing MIDI files
//***************************************************************************
// MIDI to ESF conversion tool
// Copyright 2012 Javier Degirolmo
//
// This file is part of midi2esf.
//
// midi2esf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// midi2esf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with midi2esf.  If not, see <http://www.gnu.org/licenses/>.
//***************************************************************************

// Required headers
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include "file_utils.h"
#include "midi_utils.h"
#include "midievent.h"
#include "midifile.h"

//#include "..\genres.h"
extern /* BOOL */ unsigned char verbose;


// Information for MIDI timing
// More complex than it could due to SMPTE...
typedef struct {
   unsigned smpte: 1;   // 0 = ticks per beat, 1 = SMPTE
   uint32_t ticks;      // Ticks per beat (normal) or per frame (SMPTE)
   uint32_t speed;      // Tempo (normal) or frames per 100 seconds (SMPTE)
   uint64_t last;       // Last timestamp (0x10000 = 1 frame)
} MidiTiming;

// Private function prototypes
static void calculate_timestamp(int32_t, MidiTiming *);
static int calculate_volume(int, int);
static MidiTiming timing;

// A list of MIDI-to-Echo channel mappings
static int channel_map[NUM_MIDICHAN];

// A list of MIDI-to-Echo instrument mappings
// There are different lists for each instrument
static struct {
   int instrument;      // Echo instrument (-1 = not mapped)
   int transpose;       // Transpose (in semitones)
   int volume;          // Volume scaling (percentage)
} instr_map[NUM_INSTRTYPES][NUM_MIDIINSTR];

// Status of each MIDI channel as we parse stuff
// Putting this here because otherwise it'll get lost in the middle of the
// MIDI code madness...
static struct {
   int instrument;      // Current instrument (-1 = none)
   int volume;          // Current channel volume
   int velocity;        // Current note volume
   int panning;         // Current panning
   int note;            // Last played note (-1 = none)
} status[NUM_MIDICHAN];

// Value used to convert the value of a pitch wheel event into semitones
static int pitch_factor;


//***************************************************************************
// calculate_timestamp [internal]
// Calculates the timestamp of an event (in Echo ticks!)
//---------------------------------------------------------------------------
// param delta: delta time for this event
// param timing: structure with timing information
// return: timestamp for this event (in Echo ticks, 48.16 fixed comma)
//***************************************************************************

// These constants are pretty much well-defined (and not by our program), but
// I'm naming them just to make it clearer what's the logic behind the timing
// calculations.
#define ECHO_TICKRATE 60      // Echo's tickrate (60Hz)
#define SECS_PER_MIN 60       // Seconds in a minute (60, d'oh!)

static void calculate_timestamp(int32_t delta, MidiTiming *timing) {
   // Huh, no timestamp change?
   // (checking this explicitly since it's very common)
   if (delta == 0)
      return;

   // To store how much of a difference is there from the last timestamp
   uint64_t value;

   // SMPTE timing
   // Separating this in individual lines because it's a mess... The compiler
   // will eventually notice the potential optimizations anyways, it isn't
   // that stupid as far as I know.
   if (timing->smpte) {
      value = delta;
      value *= ECHO_TICKRATE * 100 << 16;
      value /= timing->speed;
      value /= timing->ticks;
   }

   // Normal timing
   // Again, split into multiple lines to make it clearer.
   else {
      value = delta;
      value *= ECHO_TICKRATE * SECS_PER_MIN << 16;
      value /= timing->speed;
      value /= timing->ticks;
   }

   // Update timestamp
   timing->last += value;
}

//***************************************************************************
// calculate_volume [internal]
// Returns the output volume (to pass to the ESF parser) of the specified
// channel.
//---------------------------------------------------------------------------
// param midichan: MIDI channel
// param echochan: Echo channel
// return: output volume (0..127)
//***************************************************************************

static int calculate_volume(int midichan, int echochan) {
   // Calculate the actual volume based on all the parameters (thanks MIDI!)
   int output = status[midichan].volume * status[midichan].velocity / 0x7F;

   // Apply instrument's volume scaling
   if (echochan >= CHAN_FM1 && echochan <= CHAN_FM6)
   {
      output = output * instr_map[INSTR_FM][status[midichan].instrument].volume / 100;
	  if (instr_map[INSTR_FM][status[midichan].instrument].instrument == -1)
	  {
		  printf("FM instrument %d not mapped\n", status[midichan].instrument);
	  }
   }
   else if (echochan >= CHAN_PSG1 && echochan <= CHAN_PSG4EX)
   {
      output = output * instr_map[INSTR_PSG][status[midichan].instrument].volume / 100;
	  if (instr_map[INSTR_PSG][status[midichan].instrument].instrument == -1)
	  {
		  printf("PSG instrument %d not mapped\n", status[midichan].instrument);
	  }
   }
   else
      output = 0x7F;

   // Cap the volume to ensure it doesn't go past the limit
   if (output > 0x7F)
      output = 0x7F;
   if (output < 0x00)
      output = 0x00;

   // Return final volume
   return output;
}

///MIDI STUFF
static int TrkNr;
static int TrksToDo = 1;
static int Measure, M0, Beat, Clicks;
static long T0;

static int fold = 0;            /* fold long lines */
//static int times = 0;           /* print times as Measure/beat/click */


static FILE *F;

__inline int getMIDIFileByte()
{
	return(getc(F));
}



void prtime()
{
	calculate_timestamp(Mf_deltatime, &timing);

	/*
	if (times)
	{
		long m = (Mf_currtime - T0) / Beat;
		printf("(Time %ld:%ld:%ld) ", m / Measure + M0, m%Measure, (Mf_currtime - T0) % Beat);
	}
	else
		printf("(Tick %ld) ", Mf_currtime);
	*/
}

void prtext(char *p, int leng)
{
	int n, c;
	int pos = 25;

	if (!verbose)	return;


	printf("\"");
	for (n = 0; n<leng; n++) {
		c = *p++;
		if (fold && pos >= fold) {
			printf("\\\n\t");
			pos = 13;       /* tab + \xab + \ */
			if (c == ' ' || c == '\t') {
				putchar('\\');
				++pos;
			}
		}
		switch (c) {
		case '\\':
		case '"':
			printf("\\%c", c);
			pos += 2;
			break;
		case '\r':
			printf("\\r");
			pos += 2;
			break;
		case '\n':
			printf("\\n");
			pos += 2;
			break;
		case '\0':
			printf("\\0");
			pos += 2;
			break;
		default:
			if (isprint(c)) {
				putchar(c);
				++pos;
			}
			else {
				printf("\\x%.2x", c);
				pos += 4;
			}
		}
	}
	printf("\"\n");
}

void prhex(char *p, int leng)
{
	int n;
	int pos = 25;

	if (!verbose)	return;

	for (n = 0; n<leng; n++, p++) {
		if (fold && pos >= fold) {
			printf("\\\n\t%.2x", *p);
			pos = 14;       /* tab + ab + " ab" + \ */
		}
		else {
			printf(" %.2x", *p);
			pos += 3;
		}
	}
	printf("\n");

}

static void mknote(int code, int chan, int pitch, int vol)
{
	static char * Notes[] =
	{ "c", "c#", "d", "d#", "e", "f", "f#", "g",
	"g#", "a", "a#", "b" };

	if (!verbose)	return;

	if (channel_map[chan] == CHAN_PCM)
		printf("Drum %s : %d, channel %d, pitch: %d volume:%d\n", (code == 0 ? "On " : ((code == 1) ? "Off" : "Pressure")), pitch, chan + 1, pitch / 12, vol);
	else
		printf("Note %s : %s, channel %d, pitch: %d volume:%d\n", (code == 0 ? "On " : ((code == 1) ? "Off" : "Pressure")), Notes[pitch % 12], chan + 1, pitch / 12, vol);

}


void onError(char *s)
{
	if (TrksToDo <= 0)
		fprintf(stderr, "Error: Garbage at end %s\n", s);
	else
		fprintf(stderr, "Error: %s\n", s);
}

void onHeader(int format, int ntrks, int division)
{
	memset(&timing, 0, sizeof(timing));
	/*
	if (division & 0x8000) // SMPTE
	{
		times = 0;           // Can't do beats
		printf("MFile type %d, %d tracks, %d %d\n", format, ntrks, -((-(division >> 8)) & 0xff), division & 0xff);
	}
	else
		printf("MFile type %d, %d tracks, %d\n", format, ntrks, division);

	if (format > 2) {
		fprintf(stderr, "Can't deal with format %d files\n", format);
		exit(1);
	}
	Beat = Clicks = division;
	TrksToDo = ntrks;
	*/

	if (format > 2) {
		//errcode = ERR_MIDITYPE2;
		printf("Can't deal with format %d files\n", format);
		return;
	}

	if (division & 0x8000) {
		// SMPTE timing, use SMPTE timecode
		timing.smpte = 1;
		timing.ticks = division & 0xFF;

		// Determine framerate
		// Only a few framerates are allowed
		// Also WTF at 29 actually being 29.97
		switch ( (division>>8) & 0x7F)
		{
			case 24: timing.speed = 2400; break;
			case 25: timing.speed = 2500; break;
			case 29: timing.speed = 2997; break;
			case 30: timing.speed = 3000; break;

				// Oops, invalid timing!
			default:
				/*
				errcode = ERR_CORRUPT;
				goto error;
				*/
				return;
		}
	}
	else
	{
		// Normal timing, specify in ticks per beat
		timing.smpte = 0;
		timing.ticks = division;
		timing.speed = 120;
	}
}

void onTrackStart()
{
    unsigned i;
	if (verbose)
		printf("**** Track start ****\n");
	TrkNr++;

	// Reset timestamp
	// Each track starts at the first frame
	timing.last = 0;

	// Reset the status of all MIDI channels
	// This is just internal to send extra information for each event. That
	// said, this method can lead to issues with some unusual MIDIs (though
	// should work with most MIDIs out there). The issue is that the proper
	// way to handle MIDIs is to parse all tracks simultaneously...
	for (i = 0; i < NUM_MIDICHAN; i++)
	{
		status[i].instrument = -1;
		status[i].volume = 0x7F;
		status[i].velocity = 0x7F;
		status[i].panning = 0x40;
		status[i].note = -1;
	}

}

void onTrackEnd()
{
	if (verbose)
		printf("**** Track end ****\n");
	--TrksToDo;
}

//0x8x
void onNoteOff(int chan, int pitch, int vol)
{
	prtime();
	mknote(1, chan, pitch, vol);

	if (channel_map[chan] == CHAN_NONE)
	{
		printf("MIDI Channel %d not mapped to an Echo channel\n", chan);
		return;
	}

	Event *e = add_event(timing.last);
	if (e == NULL) {
		//errcode = ERR_NOMEMORY;
		//goto error;
		return;
	}
	e->type = EVENT_NOTEOFF;
	e->channel = channel_map[chan];

}

//0x9x
void onNoteOn(int chan, int pitch, int vol)
{
	if (vol == 0)
	{
		onNoteOff(chan, pitch, vol);
		return;
	}

	prtime();
	mknote(0, chan, pitch, vol);

	// Get target channel
	int channel = channel_map[chan];
	if (channel == CHAN_NONE)
	{
		printf("MIDI Channel %d not mapped to an Echo channel\n", chan);
		return;
	}

	// Get note to play
	int note = pitch;

	// Calculate volume
	status[chan].velocity = vol;
	int volume = calculate_volume(chan, channel);

	// Get instrument to use
	int instrument = -1;
	if (channel >= CHAN_FM1 && channel <= CHAN_FM6)
	{
		instrument = instr_map[INSTR_FM][status[chan].instrument].instrument;
		note += instr_map[INSTR_FM][status[chan].instrument].transpose;

		if (instrument == -1)
		{
			printf("FM instrument %d not mapped\n", status[chan].instrument);
		}
	}
	else if (channel >= CHAN_PSG1 && channel <= CHAN_PSG4EX)
	{
		instrument = instr_map[INSTR_PSG][status[chan].instrument].instrument;
		note += instr_map[INSTR_PSG][status[chan].instrument].transpose;

		if (instrument == -1)
		{
			printf("PSG instrument %d not mapped\n", status[chan].instrument);
		}
	}
	else if (channel == CHAN_PCM)
	{
		instrument = instr_map[INSTR_PCM][note].instrument;

		if (instrument == -1)
		{
			printf("PCM note %d not mapped\n", note);
		}
	}

	// Issue a note on event for this channel
	Event *e = add_event(timing.last);
	if (e == NULL) {
		//errcode = ERR_NOMEMORY;
		//goto error;
		return;
	}
	e->type = EVENT_NOTEON;
	e->param = (channel == CHAN_PCM) ? instrument : note;
	e->channel = channel;
	e->instrument = instrument;
	e->volume = volume;
	e->panning = status[chan].panning;

	// Keep track of which semitone is playing (needed for slides
	// to work properly)
	status[chan].note = note << 4;
}

//0xAx
void onNoteAfterTouch(int chan, int pitch, int press)
{
	prtime();
	mknote(2, chan, pitch, press);


	// Get target channel
	int channel = channel_map[chan];
	if (channel == CHAN_NONE)
	{
		printf("MIDI Channel %d not mapped to an Echo channel\n", chan);
		return;
	}

	// Calculate new volume
	status[chan].velocity = press;
	int volume = calculate_volume(chan, channel);

	// Issue a volume change event for this channel
	Event *e = add_event(timing.last);
	if (e == NULL) {
		//errcode = ERR_NOMEMORY;
		//goto error;
		return;
	}
	e->channel = channel;
	e->type = EVENT_VOLUME;
	e->param = volume;
}

//0xBx
void onMidiCC(int chan, int control, int value)
{
	prtime();

	//TODO : MIDICC
	if (verbose)
		printf("MIDICC channel %d, control %d (0x%.2x), value %d (0x%.2x)\n", chan + 1, control, control, value, value);
}

//0xCx
void onProgramChange(int chan, int program)
{
	prtime();

	if (verbose)
		printf("Program Change channel %d  %d\n", chan + 1, program);

	// Store new instrument
	status[chan].instrument = program;
}

//0xDx
void onChanelAfterTouch(int chan, int press)
{
	prtime();

	if (verbose)
		printf("Channel AfterTouch channel %d  v=%d\n", chan + 1, press);

	// Get target channel
	int channel = channel_map[chan];
	if (channel == CHAN_NONE)
	{
		printf("MIDI Channel %d not mapped to an Echo channel\n", chan);
		return;
	}

	// Calculate new volume
	status[chan].velocity = press;
	int volume = calculate_volume(chan, channel);

	// Issue a volume change event for this channel
	Event *e = add_event(timing.last);
	if (e == NULL) {
		//errcode = ERR_NOMEMORY;
		//goto error;
	}
	e->channel = channel;
	e->type = EVENT_VOLUME;
	e->param = volume;

}

//0xEx
void onPitchBend(int chan, int lsb, int msb)
{
	prtime();

	if (verbose)
		printf("Pitch Bend channel %d  v=%d\n", chan + 1, 128 * msb + lsb);

	// Get target channel
	int channel = channel_map[chan];
	if (channel == CHAN_NONE)
	{
		printf("MIDI Channel %d not mapped to an Echo channel\n", chan);
		return;
	}

	// Calculate note to play (in 1/16ths of a semitone)
	int wheel = msb << 7 | lsb;
	/*int note = status[chan].note + (wheel - 0x2000) / 0x100;*/
	int note = status[chan].note + (wheel - 0x2000) / pitch_factor;

	// Issue a pitch change event for this channel
	Event *e = add_event(timing.last);
	if (e == NULL) {
		//errcode = ERR_NOMEMORY;
		//goto error;
		return;
	}
	e->channel = channel;
	e->type = EVENT_SLIDE;
	e->param = note;

}


//0xF0
void onSysEx(int leng, char *mess)
{
	prtime();

	if (verbose)
	{
		printf("SysEx ");
		prhex(mess, leng);
	}
}
//oxF7
void onArbitrarySysEx(int leng, char *mess)
{
	prtime();

	if (verbose)
	{
		printf("ArbitrarySysEx ");
		prhex(mess, leng);
	}
}

//OxFF 0
void onMetaSeqNumber(int num)
{
	prtime();

	if (verbose)
		printf("SeqNumber %d\n", num);
}

//0xFF 1->7
void onMetaText(int type, int leng, char *mess)
{
	static char *ttype[] = {
		NULL,
		"Text",         /* type=0x01 */
		"Copyright",    /* type=0x02 */
		"TrkName",
		"InstrName",    /* ...       */
		"Lyric",
		"Marker",
		"Cue",          /* type=0x07 */
		"Unrec"
	};
	int unrecognized = (sizeof(ttype) / sizeof(char *)) - 1;

	prtime();

	//TODO

	if (type < 1 || type > unrecognized)
	{
		if (verbose)
			printf("Meta 0x%.2x\n", type);
	}
	else if (type == 3 && TrkNr == 1)
	{
		if (verbose)
			printf("Meta SeqName\n");
	}
	else
	{
		if (verbose)
			printf("Meta %s\n", ttype[type]);
	}

	if (verbose)	prtext(mess, leng);
}

//0xFF 0x20
//TODO

//0xFF 0x2F
void onMetaEndTrack()
{
	prtime();

	if (verbose)
		printf("Meta TrkEnd\n");
}

//0xFF 0x51
void onMetaSetTempo(long tempo)
{
	prtime();

	if (verbose)
		printf("Tempo %ld\n", tempo);
}


//0xFF 0x54
void onMetaSMPTEOffset(int hr, int mn, int se, int fr, int ff)
{
	prtime();

	if (verbose)
		printf("SMPTE %d %d %d %d %d\n", hr, mn, se, fr, ff);

	if (timing.last != 0)	return;
}

//0xFF 0x58
void onMetaTimeSignature(int nn, int dd, int cc, int bb)
{
	int denom = 1;
	while (dd-- > 0)
		denom *= 2;
	prtime();

	if (verbose)
		printf("TimeSig %d/%d %d %d\n",	nn, denom, cc, bb);
	M0 += (Mf_currtime - T0) / (Beat*Measure);
	T0 = Mf_currtime;
	Measure = nn;
	Beat = 4 * Clicks / denom;
}

//0xFF 0x59
void onMetaKeySignature(int sf, int mi)
{
	prtime();
	if (verbose)
		printf("KeySig %d %s\n", (sf>127 ? sf - 256 : sf), (mi ? "minor" : "major"));
}

//0xFF 0x7F
void onMetaSpecific(int leng, char *mess)
{
	prtime();

	if (verbose)
	{
		printf("SeqSpec ");
		prhex(mess, leng);
	}
}

//0xFF 0x??
void onUnknownMeta(int type, int leng, char *mess)
{
	prtime();

	if (verbose)
	{
		printf("Meta 0x%.2x", type);
		prhex(mess, leng);
	}
}




static void initfuncs()
{
	Mf_getc = getMIDIFileByte;
	Mf_error = onError;
	Mf_header = onHeader;
	Mf_starttrack = onTrackStart;
	Mf_endtrack = onTrackEnd;

	Mf_off = onNoteOff;
	Mf_on = onNoteOn;
	Mf_pressure = onNoteAfterTouch;
	Mf_parameter = onMidiCC;
	Mf_program = onProgramChange;
	Mf_chanpressure = onChanelAfterTouch;
	Mf_pitchbend = onPitchBend;

	Mf_sysex = onSysEx;
	Mf_arbitrary = onArbitrarySysEx;

	Mf_seqnum = onMetaSeqNumber;
	Mf_text = onMetaText;
	Mf_eot = onMetaEndTrack;
	Mf_tempo = onMetaSetTempo;
	Mf_smpte = onMetaSMPTEOffset;
	Mf_timesig = onMetaTimeSignature;
	Mf_keysig = onMetaKeySignature;
	Mf_sqspecific = onMetaSpecific;
	Mf_metamisc = onUnknownMeta;

}




//***************************************************************************
// read_midi
// Reads the data from a MIDI file and gathers the events from it.
//---------------------------------------------------------------------------
// param filename: input filename
// return: error code
//---------------------------------------------------------------------------
// Sorry for all the #ifdef DEBUG madness, but trying to spot out errors in a
// MIDI parser is really hard (it's very easy to miss something). If we find
// a MIDI that is considered corrupt by the tool but isn't corrupt, we can
// run it through a debug build to see what's wrong exactly.
//***************************************************************************

int read_midi(const char *filename) {
	// To store error codes
	int errcode = ERR_NONE;

	// Initialize event list
	reset_events();

	// Open input file
	F = fopen(filename, "rb");
	if (F == NULL)
	{
		printf("Can't open %s\n", filename);
		return ERR_OPENMIDI;
	}

	initfuncs();
	set_pitch_range(2);

	TrkNr = 0;
	Measure = 4;
	Beat = 96;
	Clicks = 96;
	T0 = 0;
	M0 = 0;



	// Parse timing information
	// I so hate this part...
	/*
	MidiTiming timing;
	if (chunk.data[4] & 0x80) {
	// SMPTE timing, use SMPTE timecode
	timing.smpte = 1;
	timing.ticks = chunk.data[5];

	// Determine framerate
	// Only a few framerates are allowed
	// Also WTF at 29 actually being 29.97
	switch (chunk.data[4] & 0x7F) {
	case 24: timing.speed = 2400; break;
	case 25: timing.speed = 2500; break;
	case 29: timing.speed = 2997; break;
	case 30: timing.speed = 3000; break;

	// Oops, invalid timing!
	default:
	#ifdef DEBUG
	fprintf(stderr, "DEBUG: SMPTE framerate is %u\n",
	chunk.data[4] & 0x7F);
	#endif
	errcode = ERR_CORRUPT;
	goto error;
	}
	} else {
	// Normal timing, specify in ticks per beat
	timing.smpte = 0;
	timing.ticks = chunk.data[4] << 8 | chunk.data[5];
	timing.speed = 120;
	}
	*/

	//For each track start
	/*

	*/

	mfread();

	fclose(F);

	return errcode;
}

//***************************************************************************
// map_channel
// Maps a MIDI channel to an Echo channel.
//---------------------------------------------------------------------------
// param midichan: MIDI channel (1 to 128)
// param echochan: Echo channel (see CHAN_*)
//***************************************************************************

void map_channel(int midichan, int echochan) {

	if ((verbose) && (echochan != CHAN_NONE))
		printf("Map MIDI channel %d to Echo channel %d\n", midichan, echochan);

	channel_map[midichan - 1] = echochan;
}

//***************************************************************************
// map_instrument
// Maps a MIDI instrument to an Echo instrument.
//---------------------------------------------------------------------------
// param type: instrument type (see INSTR_*)
// param midiinstr: MIDI instrument (0 to 127)
// param echoinstr: Echo instrument (0 to 255)
// param transpose: transpose (in semitones)
// param volume: volume scale (percentage)
//***************************************************************************

void map_instrument(int type, int midiinstr, int echoinstr, int transpose, int volume) {

	if ((verbose) && (echoinstr != -1)) //avoid verbose on reset
		printf("Map MIDI instrument %d to Echo instrument %d (%s)\n", midiinstr, echoinstr, (type == INSTR_FM?"FM":((type == INSTR_PSG)?"PSG":"PCM")) );

	instr_map[type][midiinstr].instrument = echoinstr;
	instr_map[type][midiinstr].transpose = transpose;
	instr_map[type][midiinstr].volume = volume;
}

//***************************************************************************
// set_pitch_range
// Sets the range for pitch wheel. The range is given in the amount of
// semitones it can go up/down (default: 2).
//---------------------------------------------------------------------------
// param range: amount of semitones
//***************************************************************************

void set_pitch_range(int range) {
	pitch_factor = 0x200 / range;
}

//***************************************************************************
// reset_mapping
// Resel all channels and instrument mappings
void reset_mapping()
{
    unsigned i;
	// Reset all channel mappings
	// All channels are unmapped by default, except MIDI channel 10 which is
	// always mapped to Echo's PCM channel.
	for (i = 1; i <= NUM_MIDICHAN; i++)
		map_channel(i, i == 10 ? CHAN_PCM : CHAN_NONE);

	// Reset all instrument mappings
	for (i = 0; i < NUM_MIDIINSTR; i++)
	{
		map_instrument(INSTR_FM, i, -1, 0, 100);
		map_instrument(INSTR_PSG, i, -1, 0, 100);
		map_instrument(INSTR_PCM, i, -1, 0, 100);
	}
}
