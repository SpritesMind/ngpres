#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>

#include "parser.h"



struct str_parserInfo *parser_loadFromFile( FILE *hFile, long fileSize )
{
	long dwByteRead;
	char* fileContent;
	struct str_parserInfo *parseInfo;

	if ( fileSize == 0 )
	{
		sprintf(parser_error, "Can't parse an empty file");
		return NULL;
	}

	fileContent = (char *)malloc(fileSize+1);
	if (fileContent == NULL)
	{
		sprintf(parser_error, "Can't allocate %ld bytes to read file", fileSize+1);
		return NULL;
	}

    memset(fileContent, 0, fileSize+1);

    dwByteRead = fread(fileContent, fileSize, 1, hFile);
	if ( (dwByteRead == 0) && !feof(hFile))
	{
		sprintf(parser_error, "Problem reading file");
		free(fileContent);
		return NULL;
	}

	parseInfo = parser_load(fileContent);

	free(fileContent);

	return parseInfo;
}


struct str_parserInfo *parser_load( char *text )
{
	long i,j;
	long linestart,index;
	u8 spaceFound;
	char	*line;



	struct str_parserInfo *parserInfo = (struct str_parserInfo *) malloc(sizeof(struct str_parserInfo));

	parserInfo->length = strlen(text);
	if ( parserInfo->length == 0 )
	{
		sprintf(parser_error, "Can't parse an empty text");
		free(parserInfo);
		return NULL;
	}

	parserInfo->text = (char *)malloc(parserInfo->length+1);
	if (parserInfo->text == NULL)
	{
		sprintf(parser_error, "Can't allocate %ld bytes for parsing", parserInfo->length);
		free(parserInfo);
		return NULL;
	}

    memset(parserInfo->text, 0, parserInfo->length+1);
	memcpy(parserInfo->text, text, parserInfo->length);


	//convert tab in space and remove dup space
	spaceFound = FALSE;
	for (i = 0; i < parserInfo->length-1;)
	{
		if ( parserInfo->text[i] == '\t' )
			parserInfo->text[i] = ' ';

		if ( parserInfo->text[i] == ' ' )
		{
			if (spaceFound)
			{
				for (j=i+1; j< parserInfo->length; j++)
				{
					parserInfo->text[j-1] = parserInfo->text[j];
				}
				parserInfo->text[parserInfo->length-1] = '\0';
				parserInfo->length--;
			}
			else
			{
				i++;
			}

			spaceFound = TRUE;
		}
		else
		{
			spaceFound = FALSE;
			i++;
		}
	}

	//allow max possible : each byte is a line (impossible but he!)
	parserInfo->linebuffer = (long *)malloc(parserInfo->length);
	if (parserInfo->linebuffer == NULL)
	{
		free(parserInfo->text);
		sprintf(parser_error, "Can't allocate %ld bytes for line buffer", parserInfo->length);
		free(parserInfo);
		return NULL;
	}

	//find each disting line
	parserInfo->numlines = 0;
	linestart = 0;
	index = 0;
    // loop through every byte in the file
    for(i = 0; i < parserInfo->length; i++)
    {
		if(parserInfo->text[i] == '\r')
		{
			parserInfo->text[i] = '\0';
		}
		else if(parserInfo->text[i] == '\n')
		{
			parserInfo->text[i] = '\0';
		}
		else if ( (index == 0) && (parserInfo->text[i] == ' ') )
		{
			//skip
		}
		else
		{
			index++;
			continue;
		}

		if (index > 0)
		{
            // record where the line starts
            parserInfo->linebuffer[parserInfo->numlines++] = linestart;
		}
		index = 0;
        linestart = i+1;
    }
    parserInfo->linebuffer[parserInfo->numlines++] = linestart;


	//remove comment
	for (i=0; i< parserInfo->numlines; i++)
	{
		line = strstr((parserInfo->text + parserInfo->linebuffer[i]), ";");
		if (line != NULL) line[0] = '\0';
	}

	//remove end spaces
	for (i=0; i< parserInfo->numlines; i++)
	{
		line = parserInfo->text + parserInfo->linebuffer[i];
		while( strlen(line) > 0)
		{
			if ( line[ strlen(line)-1 ] == ' ')
				line[ strlen(line)-1 ] = '\0';
			else
				break;
		}
	}

	//remove empty line
	for (i=0; i< parserInfo->numlines-1; )
	{
		line = parserInfo->text + parserInfo->linebuffer[i];
		if ( strlen(line) == 0 )
		{
			for (j=i+1; j < parserInfo->numlines; j++)
				parserInfo->linebuffer[j-1] = parserInfo->linebuffer[j];

			parserInfo->numlines--;
		}
		else
		{
			i++;
		}
	}

#ifdef DEBUG
		printf("\nCleaned file:\n");
		for (i=0; i< parserInfo->numlines; i++)
		{
			line = (parserInfo->text + parserInfo->linebuffer[i]);
			printf(parserInfo->text + parserInfo->linebuffer[i]);
			printf("\n");
		}
#endif

	return parserInfo;
}


char *parser_getLine(struct str_parserInfo *parserInfo, long idx )
{
	if (idx>parserInfo->numlines)	return(NULL);

	return (parserInfo->text + parserInfo->linebuffer[idx]);
}


long parser_getLinesCount(struct str_parserInfo *parserInfo )
{
	return(parserInfo->numlines);
}

void parser_unload(struct str_parserInfo *parserInfo )
{
	free(parserInfo->linebuffer);
	free(parserInfo->text);
}


long words_count(char *string)
{
	long limit = strlen(string);
	long i, words = 0;
	u8  change = TRUE;

	for(i = 0; i < limit; ++i)
	{
		if(!isspace(string[i]))
		{
			if(change)
			{
				++words;
				change = FALSE;
			}
		}
		else
		{
			change = TRUE;
		}
	}

	return(words);
}


//// FROM http://www.cppfrance.com/code.aspx?ID=22234

char* copy( char* dst, const char* src ) {
    char* cur = dst;
    while( (*cur++ = *src++) )
        ;
    return dst;
}

char* rtrim( char* str, const char* t )
{
    char* curEnd = str, *end = str;

    char look[ 256 ] = {  1, 0 };
    while( *t )
       look[ (unsigned char)*t++ ] = 1;

    while( *end ) {
      if ( !look[ (unsigned char)*end ] )
          curEnd = end + 1;
      ++end;
    }
    *curEnd = '\0';

    return str;
}

char* ltrim( char* str, const char* t ) {
    char* curStr = NULL;

    char look[ 256 ] = { 1, 0 };
    while( *t )
        look[ (unsigned char)*t++ ] = 1;

    curStr = str;
    while( *curStr && look[ (unsigned char)*curStr ] )
        ++curStr;

    return copy( str, curStr );
}

char* trim( char* str, const char* t ) {
    return ltrim( rtrim( str, t ), t );
}


char* extractParameters( char *fullInfo )
{
	char *params = strstr( fullInfo, "}" );
	if (params != NULL)
	{
		params--; //get back to last \n
		params[0]='\0';
	}

	params = strstr( fullInfo, "{" );
	if (params != NULL)
	{
		params--; //get back to last \n
		params[0]='\0';
		params+=3;
	}

	return params;
}

