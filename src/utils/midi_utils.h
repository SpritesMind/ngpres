//***************************************************************************
// "midi.h"
// MIDI-related definitions
//***************************************************************************
// MIDI to ESF conversion tool
// Copyright 2012 Javier Degirolmo
//
// This file is part of midi2esf.
//
// midi2esf is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// midi2esf is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with midi2esf.  If not, see <http://www.gnu.org/licenses/>.
//***************************************************************************

#ifndef MIDI_H
#define MIDI_H

// Some MIDI limits
#define NUM_MIDICHAN       0x10     // Number of MIDI channels
#define NUM_MIDIINSTR      0x80     // Number of MIDI instruments

// Possible types of instrument mappings
typedef enum {
   INSTR_FM,               // FM instrument
   INSTR_PSG,              // PSG instrument
   INSTR_PCM,              // PCM instrument
   NUM_INSTRTYPES          // Number of instrument types
} InstrType;

// Error codes
enum {
	ERR_NONE,            // No error
	ERR_OPENBATCH,       // Can't open batch file
	ERR_READBATCH,       // Can't read from batch file
	ERR_OPENMIDI,        // Can't open input MIDI file
	ERR_READMIDI,        // Can't read input MIDI file
	ERR_CORRUPT,         // Corrupt MIDI file
	ERR_MIDITYPE2,       // MIDI type 2 (not supported)
	ERR_OPENESF,         // Can't open output ESF file
	ERR_WRITEESF,        // Can't write to ESF file
	ERR_NOMEMORY,        // Ran out of memory
	ERR_BADQUOTE,        // Quote inside non-quoted token
	ERR_NOQUOTE,         // Missing ending quote
	ERR_PARSE,           // Batch file has parsing errors
	ERR_UNKNOWN          // Unknown error
};


// List of logical channels (NOT Echo channels)
// Events are mapped to one of these channels
enum {
	CHAN_FM1,               // FM channel 1
	CHAN_FM2,               // FM channel 2
	CHAN_FM3,               // FM channel 3
	CHAN_FM4,               // FM channel 4
	CHAN_FM5,               // FM channel 5
	CHAN_FM6,               // FM channel 6
	CHAN_PSG1,              // PSG channel 1
	CHAN_PSG2,              // PSG channel 2
	CHAN_PSG3,              // PSG channel 3
	CHAN_PSG4,              // PSG channel 4
	CHAN_PSG4EX,            // PSG channels 3+4
	CHAN_PCM,               // PCM channel

	NUM_CHAN,               // Number of logical channels
	CHAN_NONE               // No channel assigned
};


// Function prototypes
int read_midi(const char *);
void map_channel(int, int);
void map_instrument(int, int, int, int, int);
void set_pitch_range(int);
void reset_mapping();

#endif
