#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sound_utils.h"

//#define ST_BUFSIZ 8192
//sox_sample_t buf[ST_BUFSIZ];

sox_format_t *infile, *outfile;
sox_effects_chain_t * chain;
sox_signalinfo_t interm_signal;
char* args[10];
sox_effect_t * e;





dac_format formats[] =
{
		{
				0,
				8000,
				"unknown format",
				"s8",
				""
		},
		{
				SND_FORMAT_PCM,
				32000,
				"FORMAT_PCM",
				"s8",
				"8 bit signed from 8 Khz up to 32 Khz (default)"
		},
		{
				SND_FORMAT_2ADPCM,
				22050,
				"FORMAT_2ADPCM",
				"vox",
				"SGDK's 4 bits ADPCM at 22050 Hz"
		},
		{
				SND_FORMAT_4PCM,
				16000,
				"FORMAT_4PCM",
				"s8",
				"SGDK's 8 bit signed at 16 Khz, 256 bytes boundary"

		},
		{
				SND_FORMAT_MVS,
				9346,
				"FORMAT_MVS",
				"u8",
				"8 bit unsigned at 9346Hz, max 65536 byte"
		},
		{
				SND_FORMAT_VGM,
				8000,
				"FORMAT_VGM",
				"u8",
				"Sigflup's 8 bit signed, max 8Khz"
		},
		{
				SND_FORMAT_EWF,
				10650,
				"FORMAT_EWF",
				"u8",
				"Echo's 8 bits unsigned, at 10650Hz"
		},
		{
				SND_FORMAT_XGM,
				14000,
				"FORMAT_XGM",
				"s8",
				"SGDK's 8 bits signed, at 14KHz"
		}
};



u8 sound_init(char *inputFilename, char *outputFilename, const char *sox_format)
{
	if ((infile != NULL) || (outfile != NULL))
		sound_unload();

	sox_init();

	infile = sox_open_read(inputFilename, NULL, NULL, NULL); //auto detect format, encoding, filetype
	if (infile == NULL)
	{
		printf("%s not found or invalid\n", inputFilename);
		sound_unload();
		return FALSE;
	}

	//TODO memory version
	outfile = sox_open_write(outputFilename, &(infile->signal), NULL, sox_format, NULL, NULL);
	if (outfile == NULL)
	{
		printf("Can't create %s\n", outputFilename);
		sound_unload();
		return FALSE;
	}

	interm_signal = infile->signal; /* NB: deep copy */

	chain = sox_create_effects_chain(&infile->encoding, &outfile->encoding);

	e = sox_create_effect(sox_find_effect("input"));
	args[0] = (char *)infile;
	sox_effect_options(e, 1, args);
	sox_add_effect(chain, e, &interm_signal, &infile->signal);
	free(e);


	if (verbose)
	{
		//print info
		/*
		//????
		sox_globals.output_message_handler = output_message;
		sox_globals.verbosity = 1;
		*/
		//TODO
	}
	return TRUE;
}

void sound_mono()
{
	//http://stackoverflow.com/questions/17141679/reducing-a-channel-from-wav-file-using-libsox
	outfile->signal.channels = 1;

	e = sox_create_effect(sox_find_effect("channels"));
	args[0] = "1";
	sox_effect_options(e, 1, args);
	sox_add_effect(chain, e, &interm_signal, &outfile->signal);
	free(e);
}

/*
Remove silence at start and end
*/
void sound_trim()
{
	//use silence ! not trim
	/*
	e = sox_create_effect(sox_find_effect("trim"));
	sox_add_effect(chain, e, &interm_signal, &infile->signal);
	free(e);
	*/
}


void sound_resample(int rate)
{
	char strRate[8];

	outfile->signal.rate = rate;

	if (interm_signal.rate != outfile->signal.rate) {
		sprintf(strRate, "%d", rate);

		e = sox_create_effect(sox_find_effect("rate"));
		args[0] = strRate;
		sox_effect_options(e, 1, args);
		sox_add_effect(chain, e, &interm_signal, &outfile->signal);
		free(e);
	}

}

void sound_process()
{
	e = sox_create_effect(sox_find_effect("output"));
	args[0] = (char *)outfile;
	sox_effect_options(e, 1, args);
	sox_add_effect(chain, e, &interm_signal, &outfile->signal);
	free(e);


	sox_flow_effects(chain, NULL, NULL);
}

void sound_unload()
{
	if (chain != NULL)
	{
		sox_delete_effects_chain(chain);
		chain = NULL;
	}

	if (infile != NULL)
	{
		sox_close(infile);
		infile = NULL;
	}

	if (outfile != NULL)
	{
		sox_close(outfile);
		outfile = NULL;
	}

	sox_quit();
}



