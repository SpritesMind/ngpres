#ifndef __FILE_UTILS_H__
#define __FILE_UTILS_H__

#include "..\genres.h"

#ifdef __cplusplus
extern "C" {
#endif

u8 file_isValid(char *filename);
long file_getSize(char *file);
char *file_getExtension(char *filename);
u8 file_isFileType(char *filename, char *fileType);
void file_getNewFilename(char *source, char *dest, const char *ext);

#ifdef __cplusplus
}
#endif

#endif
