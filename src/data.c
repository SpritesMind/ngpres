#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ngpres.h"
#include "utils/parser.h"
#include "utils/file_utils.h"


////////////////////////////////////////////////
///////////
/////////// PUBLIC
///////////
////////////////////////////////////////////////
u8 dataExecute(char *info, FILE *output)
{
	FILE *infile;
	char keyword[5]; //DATA\0
	char id[50];
	long dataSize, offset;
	char filename[MAX_PATH];
	char	string[MAX_PATH];
	unsigned char *data;
	int nbElem = 0;

	nbElem += sscanf(info, "%s", keyword);
	info += strlen(keyword);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "%s", id);
	info += strlen(id);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "\"%[^\"]\"", filename);
	if (nbElem < 3)
	{
		printf("Wrong DATA definition\n");
		dataHelp();
		return FALSE;
	}
	info += (strlen(filename) +2); 	//+2 for doublequote
	trim(info, EMPTY_CHAR);

	if (verbose)
	{
		printf("\n%s found\n", keyword);	 //DATA
		printf("  id   : %s \n", id);
		printf("  file : %s \n", filename);
	}

	if (!file_isValid(filename))    return FALSE;
	//read size
	infile = fopen(filename, "rb");
	if (infile == NULL)
	{
		printf("Error while opening file %s\n", filename);
		return FALSE;
	}

	fseek(infile, 0, SEEK_END);
	dataSize = ftell(infile);
	fseek(infile, 0, SEEK_SET);

	if (dataSize> 0xFFFF)
	{
		printf("Size limited to %d bytes, can't include a %ld bytes file\n", 0xFFFF, dataSize);
		fclose(infile);
		return FALSE;
		
	}
	
	
	data = (unsigned char *)malloc(dataSize);
	if (!data)
	{
		printf("Unable to load %s (%ld bytes)\n", filename, dataSize);
		fclose(infile);
		return FALSE;
	}

    memset(data, 0, dataSize);
	fread(data, 1, dataSize, infile);
	fclose(infile);
	
	sprintf(string, ";;;;;;;;;; DATA \n\talign 2\n\tpublic _%s, _%s_size\n_%s:\n", id, id, id);
	fwrite(string, strlen(string), 1, output);
	
	//write n lines (size / 4)
	for(offset =0; offset < dataSize; offset++)
	{
		//TODO fix no 4 boundary file
		if (dataSize-offset == 1)
			sprintf(string, "\tdb\t 0x%.2x\n", data[offset+0]);
		else if (dataSize-offset == 1)
			sprintf(string, "\tdb\t 0x%.2x,0x%.2x\n", data[offset+0], data[offset+1]);
		else if (dataSize-offset == 1)
			sprintf(string, "\tdb\t 0x%.2x,0x%.2x,0x%.2x\n", data[offset+0], data[offset+1], data[offset+2]);
		else
			sprintf(string, "\tdb\t 0x%.2x,0x%.2x,0x%.2x,0x%.2x\n", data[offset+0], data[offset+1], data[offset+2], data[offset+3]);
		fwrite(string, strlen(string), 1, output);
		offset+=4;
	}
	
	free(data);
	
	
	//write size
	sprintf(string, "\n\talign 2\n_%s_size:\n\tdw %d\n\n", id, (short int) (dataSize & 0xFFFF));
	fwrite(string, strlen(string), 1, output);

	return TRUE;
}

void dataHelp()
{
	printf("DATA import any raw/binary data\n");
	printf("\nBasic usage:\n");
	printf("\tDATA id \"file\"\n");

	printf("\nwhere\n");

	printf("  id\t\tresource name\n");
	printf("  file\t\tbinary file to import (size max: 65535 bytes)\n\n");

	printf("\nExample:\n");
	printf("DATA sound_driver \"resource\\sound.drv\"");
}
