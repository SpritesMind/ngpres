#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ngpres.h"

#include "utils/image_utils.h"
#include "utils/parser.h"
#include "utils/packer.h"


struct options
{
	u8 exportPal;
	u8 exportPixels;
	char packer;
	char alphaCode;
} bitmap_options;

void bitmap_convert( FILE *output, char *id  )
{
	char	string[256];

	//header
	sprintf(string, ";;;;;;;;;; BITMAP \n\talign 2\n\tpublic _%s\n_%s:\n", id, id);
	fwrite(string, strlen(string), 1, output);
//	const unsigned short *tiles;
//	const unsigned short *pal;
//	const unsigned short packedSize;
//	const unsigned short width;
//	const unsigned short height;

	//first 
	image_pack_tiles(bitmap_options.packer, 0);
	
	
	if (bitmap_options.exportPixels)
		sprintf(string, "\tdl %s_tiles\t; tiles data adress\n", id);
	else
		sprintf(string, "\tdl 0\t; NO_PIXELS\n");
	fwrite(string, strlen(string), 1, output);

	if (bitmap_options.exportPal)
		sprintf(string, "\tdl %s_pal\t; pal data adress\n", id);
	else
		sprintf(string, "\tdl 0\t; NO_PAL\n");
	fwrite(string, strlen(string), 1, output);

	if ( (bitmap_options.packer == ALGO_RAW) || (bitmap_options.exportPixels == FALSE))
		sprintf(string, "\tdw 0\t; compressed size\n\n");
	else
		sprintf(string, "\tdw 0x%x\t; compressed size\n\n", packedSize); //force word since I doubt we'll go up to 0x10000 :)
	fwrite(string, strlen(string), 1, output);

	sprintf(string, "\tdw %d\t; Width\n", nbTilesX);
	fwrite(string, strlen(string), 1, output);

	sprintf(string, "\tdw %d\t; Height\n", nbTilesY);
	fwrite(string, strlen(string), 1, output);



	//pal
	if (bitmap_options.exportPal)
	{
		sprintf(string, "%s_pal:\n", id);
		fwrite(string, strlen(string), 1, output);
		image_write_pal(output, 0);
	}

	//tiles
	if (bitmap_options.exportPixels)
	{
		sprintf(string, "\n\talign 2\n%s_tiles:\n", id);
		fwrite(string, strlen(string), 1, output);

		setbuf(stdout, NULL);

		//already done
		//image_pack_tiles(bitmap_options.packer, 0);
		if (verbose & (bitmap_options.packer != ALGO_RAW))
			printf( getCompressionResult(string, nbTiles*32, packedSize) );

		image_write_packed(output);
	}

	sprintf(string, "\n");
	fwrite(string, strlen(string), 1, output);
}


////////////////////////////////////////////////
///////////
/////////// PUBLIC
///////////
////////////////////////////////////////////////
u8 bitmapExecute(char *info, FILE *output)
{
	char keyword[7]; //BITMAP\0
	char id[50];
	char file[MAX_PATH];
	char *option;
	int nbElem = 0;

	nbElem += sscanf(info, "%s", keyword);
	info += strlen(keyword);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "%s", id);
	info += strlen(id);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "\"%[^\"]\"", file);
	if (nbElem < 3)
	{
		printf("Wrong BITMAP definition\n");
		bitmapHelp();
		return FALSE;
	}
	info += (strlen(file) +2); 	//+2 for doublequote
	trim(info, EMPTY_CHAR);

	//init default
	bitmap_options.exportPal = TRUE;
	bitmap_options.exportPixels = TRUE;
	bitmap_options.packer = ALGO_RAW;
	bitmap_options.alphaCode = ALPHA_TOPLEFT;

	if ( strlen(info) )
	{
		option = strstr(info, "NO_PAL");
		if (option != NULL)
		{
			bitmap_options.exportPal = FALSE;
		}

		option = strstr(info, "NO_PIXEL");
		if (option != NULL)
		{
			bitmap_options.exportPixels = FALSE;
		}

		option = strstr(info, "PACKER_RLE");
		if (option != NULL)
		{
			bitmap_options.packer = ALGO_RLE;
		}

		option = strstr(info, "ALPHA_NONE");
		if (option != NULL)
		{
			bitmap_options.alphaCode = ALPHA_NONE;
		}

		option = strstr(info, "ALPHA_BOTTOMRIGHT");
		if (option != NULL)
		{
			bitmap_options.alphaCode = ALPHA_BOTTOMRIGHT;
		}
	}

	if (verbose)
	{
		printf("\n%s found\n", keyword);	 //BITMAP
		printf("  id   : %s \n", id);
		printf("  file : %s \n", file);
		printf("  comp : %s \n", getCompressionName(bitmap_options.packer) );
		printf("  pal  : %d \n", bitmap_options.exportPal);
		printf("  pixel: %d \n", bitmap_options.exportPixels);
		printf("  alpha: %s \n", ((bitmap_options.alphaCode == ALPHA_NONE) ? "NONE" : ((bitmap_options.alphaCode == ALPHA_TOPLEFT) ? "TopLeft" : "BottomRight")));
	}


	if (!image_load_tiles(file, bitmap_options.alphaCode))
	{
		printf("File not found\n");
		return FALSE;
	}

	bitmap_convert(output, id);

	image_unload();

	return TRUE;
}

void bitmapHelp()
{
	printf("BITMAP lets you import a 16 or 256 colors bitmap\n");
	printf("Only the first 4 colors are imported\n");
	printf("For sprites, you should better use SPRITE and SPRITESHEET)\n");
	printf("For full screen image, see HIRES\n");
	printf("\nBasic usage:\n");
	printf("\tBITMAP id \"file\" [options]\n");

	printf("\nwhere\n");

	printf("  id\t\tresource name\n");
	printf("  file\t\tthe bitmap to convert\n");

	printf("\n  Options:\n");
	printf("  - NO_PAL\tdoesn't export pal\n");
	printf("  - NO_PIXELS\tdoesn't export tiles, only bitmap info\n");
	printf("\n  Packing options:\n");
	printf("  - PACKER_NONE\tdoesn't pack data (default)\n");
//	printf("  - PACKER_RLE\tRLE packing\n");
	printf("\n  Alpha options:\n");
	printf("  - ALPHA_TOPLEFT\tuse pixel at top left as transparent color (default)\n");
	printf("  - ALPHA_BOTTOMRIGHT\tuse pixel at top left as transparent color\n");
	printf("  - ALPHA_NONE\t\tkeep color 0 as transparent color\n\n");

	printf("\nExample:\n");
	printf("BITMAP main_logo \"resource\\logo.bmp\" PACKER_NONE ALPHA_NONE");
}
