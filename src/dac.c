#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "genres.h"
#include "utils/file_utils.h"
#include "utils/sound_utils.h"
#include "utils/parser.h"


struct options
{
	dac_format *format;
	int rate;
	char trim;
} dac_options;

static u8 processFile( FILE *output, char *id, char *filename  )
{
	char outputFilename[MAX_PATH];
	char string[MAX_PATH];

	unsigned char *buffer;
	unsigned char *ptr;
	size_t i, buffer_size;
	FILE *outfile;
	FILE *infile;
	char ext[32];

	sprintf(ext, "_%s.raw", dac_options.format->name);

	//TODO : use a temp file
	file_getNewFilename(filename, outputFilename, ext);

	if (!sound_init(filename,outputFilename, dac_options.format->sox_format))
	{
		return FALSE;
	}


	if (dac_options.trim)
		sound_trim();

	sound_mono();

	sound_resample(dac_options.rate);

	sound_process( );

	if (verbose)
		printf("\nExport to %s\n", outputFilename);

	sound_unload();

	infile = fopen(outputFilename, "rb");
	if (infile == NULL)
	{
		printf("Error while create raw file %s\n", outputFilename);
		return FALSE;
	}

	fseek(infile, 0, SEEK_END);
	buffer_size = ftell(infile);
	fseek(infile, 0, SEEK_SET);

	buffer = (unsigned char *)malloc(buffer_size);
    memset(buffer, 0, buffer_size);

	fread(buffer, 1, buffer_size, infile);
	fclose(infile);
	remove(outputFilename);


	outfile = fopen(outputFilename, "wb");
	if (outfile == NULL) {
		printf("Can't create %s\n", outputFilename);
		return FALSE;
	}
	switch (dac_options.format->formatID)
	{
		case SND_FORMAT_PCM:
		case SND_FORMAT_2ADPCM:
		case SND_FORMAT_4PCM:
		case SND_FORMAT_MVS:
		case SND_FORMAT_VGM:
		case SND_FORMAT_XGM:
			fwrite(buffer, 1, buffer_size, outfile);
			break;
		case SND_FORMAT_EWF:
			ptr = buffer;
			for (i = 0; i < buffer_size; i++, ptr++)
			{
				if (*ptr == 0xFF)
					*ptr = 0xFE;
			}

			fwrite(buffer, 1, buffer_size, outfile);
			fputc(0xFF, outfile);
			break;
	}

	if (buffer)
		free(buffer);

	fclose(outfile);

	if ( (dac_options.format->formatID == SND_FORMAT_MVS) && (buffer_size > 65536) )
	{
		printf("MVS sound should be 65536 bytes max and not %d bytes\n", buffer_size);
		return FALSE;
	}

	if (dac_options.format->formatID == SND_FORMAT_4PCM)
		sprintf(string, ";;;;;;;;;; DAC\n\t.align 256\n\t.globl %s\n%s:\n", id, id);
	else
		sprintf(string, ";;;;;;;;;; DAC\n\t.align 2\n\t.globl %s\n%s:\n", id, id);
	fwrite(string, strlen(string), 1, output);
	sprintf(string, "\tINCBIN \"%s\"\n", outputFilename);
	fwrite(string, strlen(string), 1, output);
	sprintf(string, "\n\t.set %s_len, .-%s\n", id, id);
	fwrite(string, strlen(string), 1, output);

	sprintf(string, "\n\t.align 2\n\t.globl %s_size\n%s_size:\n\tdc.l %s_len\n\n", id, id, id);
	fwrite(string, strlen(string), 1, output);

	return TRUE;
}


////////////////////////////////////////////////
///////////
/////////// PUBLIC
///////////
////////////////////////////////////////////////
u8 dacExecute(char *info, FILE *output)
{
	char keyword[4]; //DAC\0
	char id[50];
	char buffer[10];
	char file[MAX_PATH];
	char *option;
	int nbElem = 0;
	int freq;

	nbElem += sscanf(info, "%s", keyword);
	info += strlen(keyword);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "%s", id);
	info += strlen(id);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "\"%[^\"]\"", file);
	if (nbElem < 3)
	{
		printf("Wrong DAC definition\n");
		dacHelp();
		return FALSE;
	}
	info += (strlen(file) +2); 	//+2 for doublequote
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "%d", &freq);
	info += sprintf(buffer, "%d", freq);
	trim(info, EMPTY_CHAR);

	//init default
	dac_options.format =  &formats[SND_FORMAT_PCM];
	dac_options.rate = freq;
	dac_options.trim = FALSE;

	if ( strlen(info) )
	{
		option = strstr(info, "TRIM");
		if (option != NULL)
		{
			dac_options.trim = TRUE;
		}

		//options FORMAT_xxx
		option = strstr(info, "FORMAT_PCM");
		if (option != NULL)
		{
			dac_options.format = &formats[SND_FORMAT_PCM];
			if ( dac_options.rate == 0)
				dac_options.rate = dac_options.format->defaultRate;

			//see end on function
			/*
			if ((dac_options.rate < 8000) || (dac_options.rate > 32000))
			{
				printf("Wrong DAC rate value (%dHz)\n", dac_options.rate);
				dacHelp();
				return FALSE;
			}
			*/
		}

		option = strstr(info, "FORMAT_2ADPCM");
		if (option != NULL)
		{
			dac_options.format =  &formats[SND_FORMAT_2ADPCM];
			dac_options.rate = dac_options.format->defaultRate; //one and only rate available, force it
		}

		option = strstr(info, "FORMAT_4PCM");
		if (option != NULL)
		{
			dac_options.format =  &formats[SND_FORMAT_4PCM];
			dac_options.rate = dac_options.format->defaultRate; //one and only rate available, force it
		}


		option = strstr(info, "FORMAT_MVS");
		if (option != NULL)
		{
			dac_options.format =  &formats[SND_FORMAT_MVS];
			dac_options.rate = dac_options.format->defaultRate; //one and only rate available, force it
		}

		option = strstr(info, "FORMAT_VGM");
		if (option != NULL)
		{
			dac_options.format =  &formats[SND_FORMAT_VGM];
			if ( dac_options.rate == 0)
				dac_options.rate = dac_options.format->defaultRate;
			else if ( dac_options.rate > 8000 )
			{
				printf("Wrong DAC rate value (%dHz)\n", dac_options.rate);
				dacHelp();
				return FALSE;
			}
		}

		option = strstr(info, "FORMAT_EWF");
		if (option != NULL)
		{
			dac_options.format =  &formats[SND_FORMAT_EWF];
			dac_options.rate = dac_options.format->defaultRate; //one and only rate available, force it
		}

		option = strstr(info, "FORMAT_XGM");
		if (option != NULL)
		{
			dac_options.format =  &formats[SND_FORMAT_XGM];
			dac_options.rate = dac_options.format->defaultRate; //one and only rate available, force it
		}
	}


	if (dac_options.rate == 0)
	{
		//when no option, use defautl one (PCM) and adjust rate
		dac_options.rate = dac_options.format->defaultRate;
	}
	else if ( (dac_options.format->formatID == SND_FORMAT_PCM) && ((dac_options.rate < 8000) || (dac_options.rate > 32000)) )
	{
		//handle when rate only was given, not format (so used default format PCM)
		printf("Wrong DAC rate value (%dHz)\n", dac_options.rate);
		dacHelp();
		return FALSE;
	}


	if (verbose)
	{
		printf("\n%s found\n", keyword);	 //DAC
		printf("  id   : %s \n", id);
		printf("  file : %s \n", file);
		printf("  rate : %d Hz \n", dac_options.rate );
		printf("  type : %s \n", dac_options.format->name);
		printf("  trim : %s \n", (dac_options.trim?"Yes":"No"));
	}

	processFile(output, id, file);

	return TRUE;
}

void dacHelp()
{
	int i;
	printf("DAC convert (almost) any sound format to a SGDK supported one\n");
	printf("For SFX/PCM sound, not music (see ECHO_BGM)\n");
	printf("\nBasic usage:\n");
	printf("\tDAC id \"file\" rate [options]\n");

	printf("\nwhere\n");

	printf("  id\t\tresource name\n");
	printf("  file\t\tsound to convert\n");
	printf("  rate\t\tsample rate in hertz (0 = best)\n");

	printf("\n  Options (output format):\n");

	for (i=SND_FORMAT_PCM; i <= SND_FORMAT_XGM; i++)
	{
		printf("  - %s\t%s\n", formats[i].name, formats[i].help);
	}

	printf("\n  Options:\n");
	printf("  - TRIM\tremove silent\n\n");

	printf("\nExample:\n");
	printf("DAC sfx_coin \"resource\\sonic_ring.wav\" FORMAT_XGM");

}
