#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "genres.h"
#include "utils/parser.h"
#include "utils/file_utils.h"

#define BUTTON_UP       0x0001
#define BUTTON_DOWN     0x0002
#define BUTTON_LEFT     0x0004
#define BUTTON_RIGHT    0x0008
#define BUTTON_A        0x0040
#define BUTTON_B        0x0010
#define BUTTON_C        0x0020
#define BUTTON_START    0x0080
#define BUTTON_X        0x0400
#define BUTTON_Y        0x0200
#define BUTTON_Z        0x0100
#define BUTTON_MODE     0x0800



//TODO: handle sub move ?
//TODO: handle PRESSED_XXX, RELEASED_XXX, LONG_XXXX ?

typedef struct {
  int *array;
  char *moves;
  int ID;
  size_t used;
  size_t size;
} KeyArray;

typedef struct {
  KeyArray **array;

  size_t used;
  size_t size;
} Array;

Array *specialMoves;

//http://stackoverflow.com/questions/3536153/c-dynamically-growing-array


void initKeyArray(KeyArray *a, size_t initialSize) {
  a->array = (int *)malloc(initialSize * sizeof(int));
  a->used = 0;
  a->size = initialSize;
}

void insertKeyArray(KeyArray *a, int element) {
  if (a->used == a->size) {
    a->size *= 2;
    a->array = (int *)realloc(a->array, a->size * sizeof(int));
  }
  a->array[a->used++] = element;
}

void freeKeyArray(KeyArray *a) {
  free(a->array);
  a->array = NULL;

  free(a->moves);
  a->moves = NULL;

  a->used = a->size = 0;
}



void initArray(Array *a, size_t initialSize) {
  a->array = (KeyArray **)malloc(initialSize * sizeof(KeyArray *));
  a->used = 0;
  a->size = initialSize;
}

void insertArray(Array *a, KeyArray *keyArray) {
  if (a->used == a->size) {
    a->size *= 2;
    a->array = (KeyArray **)realloc(a->array, a->size * sizeof(KeyArray *));
  }
  a->array[a->used++] = keyArray;
}

void freeArray(Array *a) {

  size_t i;
  for (i=0; i < a->used; i++)
	  freeKeyArray( a->array[i] );

  free(a->array);
  a->array = NULL;
  a->used = a->size = 0;
}


u8 controls_addFromFile(char *filename);

static int handleKey( char *text )
{
	char *key;
	int ret = 0;

	key = (char *) malloc(strlen(text)+1);
	key = trim(text, " \t");

	if ( strcmp(key, "UP") == 0 )
	{
		ret |= BUTTON_UP;
	}
	else if ( strcmp(key, "DOWN") == 0 )
	{
		ret |= BUTTON_DOWN;
	}
	else if ( strcmp(key, "LEFT") == 0 )
	{
		ret |= BUTTON_LEFT;
	}
	else if ( strcmp(key, "RIGHT") == 0 )
	{
		ret |= BUTTON_RIGHT;
	}
	else if ( strcmp(key, "A") == 0 )
	{
		ret |= BUTTON_A;
	}
	else if ( strcmp(key, "B") == 0 )
	{
		ret |= BUTTON_B;
	}
	else if ( strcmp(key, "C") == 0 )
	{
		ret |= BUTTON_C;
	}
	else if ( strcmp(key, "START") == 0 )
	{
		ret |= BUTTON_START;
	}
	else if ( strcmp(key, "X") == 0 )
	{
		ret |= BUTTON_X;
	}
	else if ( strcmp(key, "Y") == 0 )
	{
		ret |= BUTTON_Y;
	}
	else if ( strcmp(key, "Z") == 0 )
	{
		ret |= BUTTON_Z;
	}
	else if ( strcmp(key, "MODE") == 0 )
	{
		ret |= BUTTON_MODE;
	}

	return ret;
}

static int handleKeyCode( char *text)
{
	int ret = 0;
	int items_read;
	int nbChar;
	char field [ 256 ];
	const char *ptr = text;

	while ( *ptr == ' ' )
    {
          ++ptr;
    }


	while (*ptr != '\0') {
		memset(field, 0, 256);
        items_read = sscanf(ptr, "%256[^+]%n", field, &nbChar);

        if (items_read == 1)
		{
			ret += handleKey(field);
            ptr += nbChar; /* advance the pointer by the number of characters read */
		}
        if ( *ptr != '+' ) {
            break; /* didn't find an expected delimiter, done? */
        }
        ++ptr; /* skip the delimiter */

		//skip space and additional delimiter
		while ( (*ptr == ' ') || (*ptr == '+')  )
		{
          ++ptr;
		}
    }

	return ret;
}

static KeyArray *handleLine( char *text )
{
	int items_read;
	int nbChar;
	char field [ 256 ];
	char *ptr = text;
	char importFile[256];
	int id;
	KeyArray *a;

	if ( strncmp(ptr, "IMPORT" , strlen("IMPORT")) == 0 )
	{
		//printf("TODO : %s\n", ptr);
		ptr += strlen("IMPORT");
		trim(ptr, EMPTY_CHAR);

		sscanf(ptr, "\"%[^\"]\"", importFile);

		controls_addFromFile(importFile);
		return NULL;
	}
	if ( strncmp(ptr, "SEQUENCE" , strlen("SEQUENCE")) == 0 )
	{
		ptr += strlen("SEQUENCE");
	}


	a = (KeyArray *) malloc(sizeof(KeyArray));
	initKeyArray(a, 1);

	trim(ptr, EMPTY_CHAR);

	items_read = sscanf(ptr, "%d%n", &id, &nbChar);
	if (items_read != 1)	return NULL;
	a->ID = id;

	//skip ID
	ptr += nbChar;
	while ( *ptr == ' ' )
    {
          ++ptr;
    }

	a->moves = (char *) malloc( strlen(ptr)+1 );
    memset(a->moves, 0, strlen(ptr)+1);
	strcpy(a->moves, ptr);

	if (strncmp(ptr,"KONAMI", 6) == 0)
	{
		insertKeyArray(a, handleKeyCode("UP"));
		insertKeyArray(a, handleKeyCode("UP"));
		insertKeyArray(a, handleKeyCode("DOWN"));
		insertKeyArray(a, handleKeyCode("DOWN"));
		insertKeyArray(a, handleKeyCode("LEFT"));
		insertKeyArray(a, handleKeyCode("RIGHT"));
		insertKeyArray(a, handleKeyCode("LEFT"));
		insertKeyArray(a, handleKeyCode("RIGHT"));
		insertKeyArray(a, handleKeyCode("B"));
		insertKeyArray(a, handleKeyCode("A"));
		return a;
	}

	if (strncmp(ptr,"SONIC", 5) == 0)
	{
		insertKeyArray(a, handleKeyCode("UP"));
		insertKeyArray(a, handleKeyCode("DOWN"));
		insertKeyArray(a, handleKeyCode("LEFT"));
		insertKeyArray(a, handleKeyCode("RIGHT"));
		insertKeyArray(a, handleKeyCode("A"));
		insertKeyArray(a, handleKeyCode("B"));
		insertKeyArray(a, handleKeyCode("C"));
		insertKeyArray(a, handleKeyCode("START"));
		return a;
	}

	while (*ptr != '\0') {
		memset(field, 0, 256);

        items_read = sscanf(ptr, "%256[^,]%n", field, &nbChar);
        if (items_read == 1)
		{
			insertKeyArray(a, handleKeyCode(field));
            ptr += nbChar; /* advance the pointer by the number of characters read */
		}
        if ( *ptr != ',' ) {
            break; /* didn't find an expected delimiter, done? */
        }
        ++ptr; /* skip the delimiter */

		//skip space and additional delimiter
		while ( (*ptr == ' ') || (*ptr == ',')  )
		{
          ++ptr;
		}
    }

	return a;
}


static int sortMove(const void *a, const void *b)
{
	const KeyArray **moveA = (const KeyArray **) a;
	const KeyArray **moveB = (const KeyArray **) b;
	int lenA = (*moveA)->used;
	int lenB = (*moveB)->used;
	int lenght = min(lenA, lenB);
	int i;

	for (i=0; i< lenght; i++)
	{
		if ( (*moveA)->array[i] < (*moveB)->array[i] )
			return -1;
		else if ( (*moveA)->array[i] > (*moveB)->array[i] )
			return 1;
	}

	//on lenght size, they share the same data so
	if (lenA == lenB)
	{
		printf("Error - Duplicate move found : \n%s\n", (*moveA)->moves);
		return 0;
	}

	if (lenA < lenB)
		return -1;
	return 1;
}


void controls_parse(struct str_parserInfo *controlsParseInfo)
{
	int i;
	KeyArray *b;

	for(i=0; i< parser_getLinesCount(controlsParseInfo); i++)
	{
		b = handleLine( (char *) parser_getLine(controlsParseInfo, i)  );
		if (b != NULL)
			insertArray(specialMoves, b);
	}
}

u8 controls_addFromFile(char *filename)
{
	FILE *in;
	long fileSize;

	struct str_parserInfo *controlsParseInfo;


    fileSize = file_getSize(filename);
    if (fileSize == 0L) return FALSE;


    in = fopen(filename, "rt"); //no NULL test needed since file_getsize was successfull
	controlsParseInfo = parser_loadFromFile( in, fileSize );
	fclose(in);

	if (controlsParseInfo == NULL)
	{
		if (verbose)	printf("%s",parser_error);
		return FALSE;
	}

	controls_parse(controlsParseInfo);

	parser_unload(controlsParseInfo);

	return TRUE;
}

u8 controls_addFromText(char *text)
{
	struct str_parserInfo *controlsParseInfo;

	controlsParseInfo = parser_load( text );
	if (controlsParseInfo == NULL)
	{
		if (verbose)	printf("%s",parser_error);
		return FALSE;
	}

	controls_parse(controlsParseInfo);

	parser_unload(controlsParseInfo);

	return TRUE;

}

void controls_convert( FILE *output, char *id  )
{
	int i,j;
	KeyArray *keys;
	char	string[256];

	/*
	printf("Before sort\n");
	for (i=0; i < specialMoves->used; i++)
	{
		keys = specialMoves->array[i];
		printf("// %s\n%d : ", keys->moves, keys->ID);

		for (j=0; j < keys->used; j++)
		{
			printf("0x%0.4x,", keys->array[j]);
		}
		printf("\n");
	}
	*/

	qsort(specialMoves->array, specialMoves->used, sizeof(KeyArray *), sortMove );

	/*
	printf("\n\nAfter sort\n");
	for (i=0; i < specialMoves->used; i++)
	{
		keys = specialMoves->array[i];
		printf("// %s\n%d : ", keys->moves, keys->ID);

		for (j=0; j < keys->used; j++)
		{
			printf("0x%0.4x,", keys->array[j]);
		}
		printf("\n");
	}
	*/

	sprintf(string, ";;;;;;;;;; CONTROLS \n\t.align 2\n\t.globl %s\n%s:\n", id, id);
	fwrite(string, strlen(string), 1, output);

	sprintf(string, "\tdc.w %d\n\tdc.l %s_sequences\n", specialMoves->used, id);
	fwrite(string, strlen(string), 1, output);

	sprintf(string, "\n%s_sequences:\n", id);
	fwrite(string, strlen(string), 1, output);

	for (i=0; i < specialMoves->used; i++)
	{
		sprintf(string, "\tdc.l %s_sequence%d\n", id, i);
		fwrite(string, strlen(string), 1, output);
	}

	for (i=0; i < specialMoves->used; i++)
	{
		keys = specialMoves->array[i];

		sprintf(string, "\n%s_sequence%d:\n\t; %s\n", id, i, keys->moves);
        fwrite(string, strlen(string), 1, output);

		sprintf(string, "\tdc.w %d\n", keys->ID);
		fwrite(string, strlen(string), 1, output);

		sprintf(string, "\tdc.w ");
        fwrite(string, strlen(string), 1, output);

		for (j=0; j < keys->used; j++)
		{
			sprintf(string, "0x%.4x,", keys->array[j]);
            fwrite(string, strlen(string), 1, output);
		}
		sprintf(string, "0x0000\n");
		fwrite(string, strlen(string), 1, output);
	}
}

////////////////////////////////////////////////
///////////
/////////// PUBLIC
///////////
////////////////////////////////////////////////
u8 controlsExecute(char *info, FILE *output)
{
	char keyword[9]; //CONTROLS\0
	char id[50];
	int nbElem = 0;
	char *sequencesParam;

	sequencesParam = extractParameters(info);

	nbElem += sscanf(info, "%s", keyword);
	info += strlen(keyword);
	trim(info, EMPTY_CHAR);

	nbElem += sscanf(info, "%s", id);
	if (nbElem < 2)
	{
		printf("Wrong CONTROLS definition\n");
		controlsHelp();
		return FALSE;
	}
	info += strlen(id);
	trim(info, EMPTY_CHAR);

	if (verbose)
	{
		printf("\n%s found\n", keyword);	 //CONTROLS
		printf("  id   : %s \n", id);
	}

	specialMoves = (Array *) malloc(sizeof(Array));
	initArray(specialMoves, 1);  // initially 1 element

	if ( (sequencesParam != NULL)  && (strlen(sequencesParam) > 0) )
		controls_addFromText(sequencesParam);


	controls_convert(output, id );

	freeArray(specialMoves);

	return TRUE;
}

void controlsHelp()
{
	printf("CONTROLS lets you define control sequence from a resource file\n");
	printf("Useful for fighting game or passcode\n");
	printf("\nBasic usage:\n");
	printf("\tCONTROLS id\n");
	printf("\t{\n");
	printf("\t\t<control or special sequence>\n");
	printf("\t\t...\n");
	printf("\t}\n");

	printf("\nwhere\n");

	printf("\n  id\t\tresource name\n");

	printf("\n  Controls:\n");
	printf("  - IMPORT \"filename\"\t import a SEQUENCE from an external file\n");
	printf("  - SEQUENCE keycodes\t define a sequence of keycodes inline, using comma\n");

	printf("  Keycodes:\n");
	printf("  - UP\tDOWN\tLEFT\tRIGHT\n");
	printf("  - A\tB\tC\tSTART\n");
	printf("  - X\tY\tZ\tMODE\n");

	printf("\n  Special sequences:\n");
	printf("  - SONIC\t alias of UDLRABCS\n");
	printf("  - KONAMI\t alias of UUDDLRLRBA\n\n");

	printf("\nExample (3 ways to define Sonic code) :\n");
	printf("CONTROLS sonic_password\n");
	printf("{\n");
	printf("  SONIC\n");
	printf("  IMPORT \"sonic_pass.key\"\n");
	printf("  SEQUENCE UP, DOWN, LEFT, RIGHT, A, B, C, START\n");
	printf("}");

}
