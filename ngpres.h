#ifndef NGPRES_H_
#define NGPRES_H_

/****
History
18/07/02	initial version, based on GenRes 2.0
18/07/11	map
*****/


typedef struct
{
	const unsigned short *tiles;
	const unsigned short *pal;
	const unsigned short packedSize;
	const unsigned short width;
	const unsigned short height;
} bitmap;

typedef struct
{
	const unsigned short *plane1;
	const unsigned short *plane2;
	const unsigned short *pal1;
	const unsigned short *pal2;
	const unsigned short packedSize1;
	const unsigned short packedSize2;
	const unsigned short width;
	const unsigned short height;
} hires;


typedef struct
{
	unsigned char rate;
	unsigned char count;
	unsigned char frames[];
} animation;

typedef struct {
	u8	top;
	u8 	left;
	u8	bottom;
	u8	right;
} collideBox;


typedef struct
{
	const unsigned short animCount;
	const unsigned char frameWidth; //pixel width 
	const unsigned char frameHeight; //pixel height
	const unsigned short frameSize; //nb tiles : (frameWidth/8) * (frameHeight/8)
	const unsigned short *pal;
	const animation **animations;
	const unsigned short **frames;
	const collideBox **collideInfos;
} sheet;


typedef struct {
	//map size, in tiles
	const unsigned short	width;
	const unsigned short	height;
	
	//pal
	const unsigned short	*pal;

	//tiles	
	const unsigned short	nbTiles; //number of tiles used
	const unsigned short	compSize; //0 if not compressed
	const unsigned short	*tiles; //pointer to tiles data
	
	//layers
	const unsigned short	nbLayers;
	const unsigned short	**layers; //pointer to layers data
} map;

#endif /* NGPRES_H_ */
