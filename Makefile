include makerules 


OBJS = utils/image_utils.o
OBJS += utils/file_utils.o
OBJS += utils/mapfunc.o
OBJS += utils/midi_utils.o
OBJS += utils/midievent.o
OBJS += utils/midifile.o
OBJS += utils/packer.o
OBJS += utils/parser.o
OBJS += utils/sound_utils.o
OBJS += bitmap.o
OBJS += hires.o
#OBJS += controls.o
#OBJS += dac.o
OBJS += data.o
OBJS += font.o
OBJS += ngpres.o
OBJS += map.o
OBJS += merge.o
OBJS += pal.o
#OBJS += psg.o
#OBJS += sprite.o
OBJS += spritesheet.o

OBJ_RELEASE = $(addprefix $(OBJDIR)/, $(OBJS))


all: libs folders $(OBJ_RELEASE) $(DEP)
	$(LD) $(LIBDIR) -o bin/ngpres $(OBJ_RELEASE) $(LDFLAGS) $(LIB)

	
	
obj/%.o: src/%.c
	$(CC) $(CFLAGS) $(INC) -c -o $@ $<


obj/utils/%.o: src/utils/%.c
	$(CC) $(CFLAGS) $(INC) -c -o $@ $<



clean:	
	@echo Clean release files and folders
	-$(RM) $(OBJ_RELEASE) $(HIDE_ERROR)
	-$(RM) bin/ngpres.* $(HIDE_ERROR)
	-$(RMD) $(OBJDIR)/utils $(HIDE_ERROR)
	-$(RMD) $(OBJDIR) $(HIDE_ERROR)
	-$(RMD) bin $(HIDE_ERROR)

folders: bin obj $(OBJDIR)/utils

bin:
	$(MKDIR) $@

obj:
	$(MKDIR) $@
	
obj/utils:
	$(MKDIR) $@

libs:  $(SOX)/src/.libs/libsox.a $(FREEIMAGE)/Dist/libFreeImage.a $(STSOUND)/libstsound.a

$(SOX)/src/.libs/libsox.a: $(SOX)/Makefile
	$(MAKE) -C $(SOX) -s	
	
$(SOX)/src/Makefile:
	$(error SoX library missing, please run configure first)
	
$(FREEIMAGE)/Dist/libFreeImage.a: $(FREEIMAGE)/Makefile.mingw
	$(MAKE) FREEIMAGE_LIBRARY_TYPE=STATIC -C $(FREEIMAGE) -f Makefile.mingw
	
$(FREEIMAGE)/Makefile.mingw:
	$(error FreeImage library missing, please run configure first)

$(STSOUND)/libstsound.a: $(STSOUND)/Makefile
	$(MAKE) -C $(STSOUND)

$(STSOUND)/Makefile:
	$(error StSound library missing, please update git submodules)


reset: clean
	$(RM) $(SOX)/src/.libs/libsox.a
	$(MAKE) -C $(SOX) clean	
	-$(RMD) $(FREEIMAGE)/Dist
	$(MAKE) -C $(FREEIMAGE) -f Makefile.mingw clean
	$(MAKE) -C $(STSOUND)
	
.PHONY: all debug release clean  folders libs reset
