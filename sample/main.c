// platform includes
#include <ngpc.h>
#include "carthdr.h"
#include <library.h>

// standard C includes
#include <stdlib.h>
#include <stdio.h>


#define	TEST_FONT		(0)
#define	TEST_BITMAP		(TEST_FONT+1)
#define TEST_HIRES		(TEST_BITMAP+1)
#define TEST_SHEET		(TEST_HIRES+1)
#define TEST_MAP		(TEST_SHEET+1)
#define TEST_DAC		(TEST_MAP+1)
#define TEST_PSG		(TEST_DAC+1)
#define TEST_CONTROL	(TEST_PSG+1)

#define	PAL_TITLE		15
#define FONT_TILE_IDX	0
//#define USER_TILE_IDX	256
//skip kana
#define USER_TILE_IDX	(8*16)

#define WORKING_ROW		24
#define WORKING_COLUMN	24

//// NGPRes stuff

typedef struct
{
	const unsigned short *tiles;
	const unsigned short *pal;
	const unsigned short packedSize;
	const unsigned short width;
	const unsigned short height;
} bitmap;

typedef struct
{
	const unsigned short *plane1;
	const unsigned short *plane2;
	const unsigned short *pal1;
	const unsigned short *pal2;
	const unsigned short packedSize1;
	const unsigned short packedSize2;
	const unsigned short width;
	const unsigned short height;
} hires;


typedef struct
{
	unsigned char rate;
	unsigned char count;
	unsigned char frames[];
} animation;

typedef struct {
	u8	top;
	u8 	left;
	u8	bottom;
	u8	right;
} collideBox;


typedef struct
{
	const unsigned short animCount;
	const unsigned char frameWidth; //pixel width 
	const unsigned char frameHeight; //pixel height
	const unsigned short frameSize; //nb tiles : (frameWidth/8) * (frameHeight/8)
	const unsigned short *pal;
	const animation **animations;
	const unsigned short **frames;
	const collideBox **collideInfos;
} sheet;


typedef struct {
	//map size, in tiles
	const unsigned short	width;
	const unsigned short	height;
	
	//pal
	const unsigned short	*pal;

	//tiles	
	const unsigned short	nbTiles; //number of tiles used
	const unsigned short	compSize; //0 if not compressed
	const unsigned short	*tiles; //pointer to tiles data
	
	//layers
	const unsigned short	nbLayers;
	const unsigned short	**layers; //pointer to layers data
} map;

////////////////////////////////////////


//#pragma pack(1)
typedef struct
{
	unsigned char CC;
	unsigned char hflip:1;
	unsigned char vflip:1;
	unsigned char k1pal:1;
	unsigned char priority:2;
	char hchain:1;
	char vchain:1;
	unsigned char CC_MSB:1;
	char x;
	char y;
} k1Sprite;
//#pragma pack()



extern const unsigned short myFont[96][8];
extern const bitmap spritesmind;
extern const unsigned short spritesmind_pal[4];
extern const hires spritesmind_hires;
extern const sheet bjack_sheet;
extern const map cybernoid;

unsigned char joyBuf;

void VDP_fillTileMapInc(u8 plane, u8 pal_id, u8 x, u8 y, u8 w, u8 h, u16 tile)
{
   u16 offset,value;
   u8 i,j;	
   
   u16 *vramAddr = SCROLL_PLANE_1;   
   if (plane == SCR_2_PLANE)	vramAddr = SCROLL_PLANE_2;

   value = tile | ((u16)pal_id << 9);

	for(j=0; j <h; j++)
	{
		offset = ((u16)(y+j) * 32) + x;
		for (i=0; i< w; i++)
		{
			vramAddr[offset+i] = value++;
		}
	}
	
}










u8 currentTest;
void testFont( )
{
	bool done = 0;
	currentTest = TEST_FONT;
	
	SysSetSystemFont();
	
	ClearScreen(SCR_1_PLANE);
	ClearScreen(SCR_2_PLANE);
	
	SetBackgroundColour(RGB(0, 0, 0));
	
	SetPalette(SCR_1_PLANE, 0, 0, RGB(10,0,0), RGB(15,0,0), RGB(0,0,15));
	VDP_fillTileMapInc(SCR_1_PLANE, 0, 0, 1, SCRN_TX+1, 1 + (256/SCRN_TX), 0);
	
	//on this sample
	//custom use color 1
	//system use color 3
	//up to dev to use same color index
	PrintString(SCR_1_PLANE, PAL_TITLE, 4, 0, "FONT SAMPLE");
	PrintString(SCR_1_PLANE, PAL_TITLE, 0, SCRN_TY-1, "A to toggle font");
	
	while(1)
	{
		WaitVsync();
		
		if ( joyBuf != JOYPAD)
		{
			joyBuf = JOYPAD;
			if (joyBuf & J_A)
			{
				if (done)
				{
					done = 0;
					SysSetSystemFont();
				}
				else
				{
					done = 1;
					InstallTileSetAt(myFont, 96*8, ' ');	
				}
			}
			else if (joyBuf & J_B)
			{
				SysSetSystemFont();
				return; //jump next
			}
		}
	}
}



void testBitmap( )
{
	currentTest = TEST_BITMAP;
	
	ClearScreen(SCR_1_PLANE);
	ClearScreen(SCR_2_PLANE);
	
	PrintString(SCR_1_PLANE, PAL_TITLE, 3, 0, "BITMAP SAMPLE");
	
	SetPalette(SCR_1_PLANE, 0, 0, RGB(10,0,0), RGB(15,0,0), RGB(0,0,15));
	InstallTileSetAt(spritesmind.tiles, spritesmind.width * spritesmind.height*8, USER_TILE_IDX);
	SetMoreColors(SCR_1_PLANE, 0, 1, spritesmind_pal+1); //+1 to skip the first 0 (see SetMoreColors)
	VDP_fillTileMapInc(SCR_1_PLANE, 0, 0, 5, spritesmind.width, spritesmind.height, USER_TILE_IDX);
	
	while(1)
	{
		WaitVsync();
		
		if ( joyBuf != JOYPAD)
		{
			joyBuf = JOYPAD;
			if (joyBuf & J_B)
			{
				return; //jump next
			}
		}
	}
}

void testHires( )
{
	currentTest = TEST_HIRES;

	ClearScreen(SCR_1_PLANE);
	ClearScreen(SCR_2_PLANE);
	
	PrintString(SCR_1_PLANE, PAL_TITLE, 4, 0, "HIRES SAMPLE");
	
	InstallTileSetAt(spritesmind_hires.plane1, spritesmind_hires.width * spritesmind_hires.height*8, USER_TILE_IDX);
	InstallTileSetAt(spritesmind_hires.plane2, spritesmind_hires.width * spritesmind_hires.height*8, USER_TILE_IDX+spritesmind_hires.width * spritesmind_hires.height);

	SetMoreColors(SCR_1_PLANE, 0, 1, spritesmind_hires.pal1+1);
	SetMoreColors(SCR_2_PLANE, 0, 1, spritesmind_hires.pal2+1);

	VDP_fillTileMapInc(SCR_1_PLANE, 0, 0, 5, spritesmind_hires.width, spritesmind_hires.height, USER_TILE_IDX);
	VDP_fillTileMapInc(SCR_2_PLANE, 0, 0, 5, spritesmind_hires.width, spritesmind_hires.height, USER_TILE_IDX+spritesmind_hires.width * spritesmind_hires.height);

	
	while(1)
	{
		WaitVsync();
		
		if ( joyBuf != JOYPAD)
		{
			joyBuf = JOYPAD;
			if (joyBuf & J_B)
			{
				return; //jump next
			}
		}
	}
}

//to get jack's mem address on ngpres.map and easy debug ;)
k1Sprite jack[5*4];
void testSheet( )
{
	unsigned char i;
	unsigned char jack_k2pal[5*4];
	unsigned char animFrame[5];
	unsigned char animTick[5];
	
	
	currentTest = TEST_SHEET;
	
	ClearScreen(SCR_1_PLANE);
	ClearScreen(SCR_2_PLANE);
	
	PrintString(SCR_1_PLANE, PAL_TITLE, 2, 0, "SPR SHEET SAMPLE");

	SetMoreColors(SPRITE_PLANE, 0, 1, bjack_sheet.pal+1);
	
	InstallTileSetAt(bjack_sheet.frames[ bjack_sheet.animations[0]->frames[0] ] , bjack_sheet.frameSize*8*5, USER_TILE_IDX);

	for(i=0; i< 5; i++)
	{
		animation *spriteAnimation = (animation *) bjack_sheet.animations[i];
		animFrame[i] = 0;
		animTick[i] = spriteAnimation->rate;
		
		InstallTileSetAt(bjack_sheet.frames[spriteAnimation->frames[animFrame[i]]] , bjack_sheet.frameSize*8, USER_TILE_IDX+(i*bjack_sheet.frameSize));
	}



	for(i=0; i< 5*4; i++)
	{
		jack[i].CC = (USER_TILE_IDX+i)&0xFF;
		jack[i].CC_MSB = ((USER_TILE_IDX+i)>>8)&0x1;
		jack[i].hflip = 0;
		jack[i].vflip = 0;
		jack[i].k1pal = 0;
		jack[i].priority = 3; //SPR_FRONT;
		jack[i].hchain=1; //(i%4!=0);
		jack[i].vchain=1; //(i%4!=0);
		jack[i].x=0;
		jack[i].y=0;
	}
	
	
	//chain :
	//sprite1 : master
	//sprite2 : sprite1.x+8, sprite1.y+0
	//sprite3 : sprite2.x-8, sprite2.y+8
	//sprite4 : sprite3.x+8, sprite3.y+0
	
	for(i=0; i< 5; i++)
	{
		jack[i*4].hchain = 0;
		jack[i*4].vchain = 0;
		


		jack[i*4].y=24*(i+1);
		
		jack[i*4+1].x=8;
		jack[i*4+2].x=-8;
		jack[i*4+3].x=8;
		
		//jack[i*4+1].y=0;
		jack[i*4+2].y=8;
		//jack[i*4+3].y=0;
	}

	BlockCopy(SPRITE_RAM, jack, 5*4*4);

	for(i=0; i< 5*4; i++)
	{
		jack_k2pal[i] = 0;
	}
	BlockCopy(SPRITE_COLOUR, jack_k2pal, 5*4);



	
	while(1)
	{
		WaitVsync();
		
		for(i=0; i< 5; i++)
		{
			animTick[i]--;
			if (animTick[i] == 0)
			{ 
				animation *spriteAnimation = (animation *) bjack_sheet.animations[i];
				animTick[i] = spriteAnimation->rate;

				animFrame[i]++;
				if (animFrame[i] == spriteAnimation->count)
					animFrame[i] = 0;
				
				InstallTileSetAt(bjack_sheet.frames[spriteAnimation->frames[animFrame[i]]] , bjack_sheet.frameSize*8, USER_TILE_IDX+(i*bjack_sheet.frameSize));
			}
		}
	
		if ( joyBuf != JOYPAD)
		{
			joyBuf = JOYPAD;
			if (joyBuf & J_B)
			{
				for(i=0; i< 5*4; i++)
				{
					UnsetSprite(i);
				}				
				
				return; //jump next
			}
		}

		if (joyBuf & (J_UP|J_DOWN|J_LEFT|J_RIGHT))
		{
			//TODO
			if (joyBuf & J_UP)
			{
				jack[0].y--;
			}
			else if (joyBuf & J_DOWN)
			{
				jack[0].y++;
			}
			else if (joyBuf & J_LEFT)
			{
				jack[0].x--;
			}
			else if (joyBuf & J_RIGHT)
			{
				jack[0].x++;
			}
				

			BlockCopy(SPRITE_RAM, jack, /*5**/4*4);
		}
			
	}
}



void drawRow(s16 scrollX, s16 scrollY)
{
	char x;
	u16 posX;
	u16 arrayIdx;
	s16 tileY = scrollY/8;
	s16 tileX = scrollX/8;
	u8 fakeL24 = WORKING_ROW;
	u8 fakeL25 = WORKING_ROW+1;
	
	tileY = scrollY/8;
	tileX = scrollX/8;
	if (tileY > 0)
	{
		fakeL24 += tileY;
		fakeL24 %= 32;
		fakeL25 += tileY;
		fakeL25 %= 32;
	}
	
	
	x = min( tileX, (32-WORKING_COLUMN)) * -1;

	//using arrayIdx & tileX avoid computing array idx 32 times
	posX = tileX+x;
	arrayIdx = tileY*cybernoid.width + posX;
	for (x; x < WORKING_COLUMN; x++)
	{ 
		PutTile(SCR_2_PLANE, 0, posX%32, fakeL24, USER_TILE_IDX + cybernoid.layers[1][arrayIdx + WORKING_ROW*cybernoid.width]); //x + (scrollX/8)  + (R24 + (scrollY/8))*cybernoid.width]);
//		if (scrollY/8 >= (32- (WORKING_ROW+1)))
		PutTile(SCR_2_PLANE, 0, posX%32, fakeL25, USER_TILE_IDX + cybernoid.layers[1][arrayIdx - ((32-WORKING_ROW-1)*cybernoid.width)]);
			
		arrayIdx++;
		posX++;
	}
}
void drawColumn(s16 scrollX, s16 scrollY)
{
	char y;
	u16 posY;
	u16 arrayIdx;
	s16 tileY = scrollY/8;
	s16 tileX = scrollX/8;
	u8 fakeC24 = WORKING_COLUMN;
	u8 fakeC25 = WORKING_COLUMN+1;

	tileY = scrollY/8;
	tileX = scrollX/8;
	if (tileX > 0)
	{
		fakeC24 += tileX;
		fakeC24 %= 32;
		fakeC25 += tileX;
		fakeC25 %= 32;
	}
	
	y = min( tileY, (32-WORKING_ROW)) * -1;
	
	//using arrayIdx & tileY avoid computing array idx 32 times
	posY = (y + tileY);
	arrayIdx = posY*cybernoid.width + tileX;
	for (y; y <= WORKING_ROW; y++)
	{ 
		PutTile(SCR_2_PLANE, 0, fakeC24, posY%32, USER_TILE_IDX + cybernoid.layers[1][arrayIdx + WORKING_COLUMN]);
//		if ((scrollX/8) >= (32- (WORKING_COLUMN+1)))
		PutTile(SCR_2_PLANE, 0, fakeC25, posY%32, USER_TILE_IDX + cybernoid.layers[1][arrayIdx - (32-WORKING_COLUMN-1)]);
		
		posY++;	
		arrayIdx += cybernoid.width;
	}
}
void testMap( )
{
	u8 x,y;
	s16 scrollX, scrollY;
	currentTest = TEST_MAP;

	ClearScreen(SCR_1_PLANE);
	ClearScreen(SCR_2_PLANE);
	
	PrintString(SCR_1_PLANE, PAL_TITLE, 5, 0, "MAP SAMPLE");

	PrintString(SCR_1_PLANE, PAL_TITLE, 0, SCRN_TY-2, "DIR TO SCROLL");
	PrintString(SCR_1_PLANE, PAL_TITLE, 0, SCRN_TY-1, "A TO SPEED");

	InstallTileSetAt(cybernoid.tiles, min(cybernoid.nbTiles, 512-USER_TILE_IDX)*8, USER_TILE_IDX);
	SetMoreColors(SCR_2_PLANE, 0, 1, cybernoid.pal+1); //+1 to skip the first 0 (see SetMoreColors)
	
	scrollX = 0;
	scrollY = 0;
	for (y=0; y < WORKING_ROW; y++)
	{
		for (x=0; x < WORKING_COLUMN; x++)
		{ 
			PutTile(SCR_2_PLANE, 0, x, y, USER_TILE_IDX + cybernoid.layers[1][x + y*cybernoid.width]);
		}
	}
	while(1)
	{
		bool dirty = 0;
		WaitVsync();
		
		if ( joyBuf != JOYPAD)
		{
			joyBuf = JOYPAD;
			if (joyBuf & J_B)
			{
				ShiftScroll(SCR_2_PLANE, 0, 0);
				return; //jump next
			}
		}

		if (joyBuf & J_UP)
		{
			if (scrollY > 0)
			{
				dirty = 1;
				--scrollY;
			}
		}
		else if (joyBuf & J_DOWN)
		{
			if ((scrollY/8) < cybernoid.height - SCRN_TY)
			{
				dirty = 1;
				++scrollY;
			}
		}
		
		if (joyBuf & J_LEFT)
		{
			if (scrollX > 0)
			{
				dirty = 1;
				--scrollX;
			}
		}
		else if (joyBuf & J_RIGHT)
		{
			if ((scrollX/8) < cybernoid.width - SCRN_TX)
			{
				dirty = 1;
				++scrollX;
			}
		}
			
		if (dirty)
		{
			//drawback of using dirty flag, game speed could be slower while scrolling
			//to get same speed each vsync, do same thing each sync ;)
			drawColumn(scrollX, scrollY);
			drawRow(scrollX, scrollY);
			ShiftScroll(SCR_2_PLANE, scrollX%(32*8), scrollY%(32*8));
			dirty = 0;
		}
	}
}
void testDAC( )
{
	currentTest = TEST_DAC;

	ClearScreen(SCR_1_PLANE);
	ClearScreen(SCR_2_PLANE);
	
//	ShiftScroll(SCR_2_PLANE, 0, 0);
				
	PrintString(SCR_1_PLANE, PAL_TITLE, 5, 0, "DAC SAMPLE");

	while(1)
	{
		WaitVsync();
		
		if ( joyBuf != JOYPAD)
		{
			joyBuf = JOYPAD;
			if (joyBuf & J_B)
			{
				return; //jump next
			}
		}
	}
}

void testPSG( )
{
	currentTest = TEST_PSG;

	ClearScreen(SCR_1_PLANE);
	ClearScreen(SCR_2_PLANE);
	
	PrintString(SCR_1_PLANE, PAL_TITLE, 5, 0, "PSG SAMPLE");

	while(1)
	{
		WaitVsync();
		
		if ( joyBuf != JOYPAD)
		{
			joyBuf = JOYPAD;
			if (joyBuf & J_B)
			{
				return; //jump next
			}
		}
	}
}

void testControl( )
{
	currentTest = TEST_CONTROL;

	ClearScreen(SCR_1_PLANE);
	ClearScreen(SCR_2_PLANE);
	
	PrintString(SCR_1_PLANE, PAL_TITLE, 3, 0, "CONTROL SAMPLE");
	PrintString(SCR_1_PLANE, PAL_TITLE, 0, 4, "KONAMI CODE ?");

	while(1)
	{
		WaitVsync();
		
		if ( joyBuf != JOYPAD)
		{
			joyBuf = JOYPAD;
			if (joyBuf & J_B)
			{
				return; //jump next
			}
		}
	}
}

void main(void)
{
	InitNGPC();
    SysSetSystemFont();

	SetBackgroundColour(RGB(0, 0, 0));
	
	SetPalette(SCR_1_PLANE, PAL_TITLE, 0, RGB(15,15,15), RGB(15,15,15), RGB(15,15,15));
	PrintString(SCR_1_PLANE, PAL_TITLE, 5, 2, "GenRes sampler");
	PrintString(SCR_1_PLANE, PAL_TITLE, 5, 3, "0.1");
	PrintString(SCR_1_PLANE, PAL_TITLE, 1, SCRN_TY-1, "B for next sample");

	joyBuf = JOYPAD;
	while(1)
	{
		WaitVsync();
		if( joyBuf != JOYPAD)
		{
			joyBuf = JOYPAD;
			if (joyBuf & J_B)
			{
				//launc tests
				break;
			}
		}
 	}

	
	while (1) {
		testFont();
		testBitmap();
		testHires();
		testSheet();
		testMap();
		//testDAC();
		//testPSG();
		//testControl();
	}
}
