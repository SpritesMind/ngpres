NGPRes 0.1
===================
NGRes is the advanced resource compiler for NeoGeo Pocket.
Output is T9000 optimized.

*by KanedaFr (c) 2018 *[SpritesMind](http://www.spritesmind.net)


History
-------------
|Version|Date|Details|
|-|-|-|
| 0.1 | 1 July 2018 | initial public commit|


----------

Usage
-------------

    ngpres <source.rc> <export.asm> [-v]


source.rc is a file defining every resource using plugins available.
Every plugin follows the same syntax

    plugin id file
    {
    	option value
    	option value
    } 
> **Tip:** included `resource.rc` is a template you can use to create your own `source.rc`

----------

Plugins
-------------

> For history reason, I still use the word "plugin" to describe a resource converter/importer/compiler.

&nbsp;

> **Note:**
> - You won't find plugin syntax here : use `ngpres help <plugin name>` to get full syntax, help and sample
> - Format of generated data is defined on ngpres.h

#### BITMAP

Convert any image to tiles.
Use only the first 4 colors, so fix your palette first !

#### HIRES

Advanced version of bitmap.
Export 2 tiles array, each one using one of the 2 pal.
Drawing tile on plane 1 and plane 2 will give you a 7 colors screen (title, gameover....)
Use only the first 7 colors, so fix your palette first !

#### FONT

minimal version of `BITMAP` to import font

#### PAL

striped down version of `BITMAP` to import pal only

#### SPRITESHEET

A more advanced feature : convert a image with several sprites to a list of sprites, with framerate, user defined animation, collide box...

> **Tip:** A lot of spritesheets is available on the internet. Use them for testing !


#### MAP

Import layers and bitmap data from [Mappy](http://www.tilemap.co.uk/mappy.php) maps

COMING SOON


#### CONTROLS

Add moves list or cheat code to your game.
Perfect for a fighting game !


COMING SOON

#### DAC

Convert sound to use with any of the SGDK supported drivers

COMING SOON

#### PSG

Import Atari, Amstrad, MSX.. tone *(no noise support)*
COMING SOON


#### DATA

Add raw data,  similar to common asm's INCBIN

----------

Samples
-------------

This sample aims to explain how to use every kind of data supported by NGPRes.
Be free to browse source code to learn how to use basis of NGPRes

*Required* : 
T9000 compiler, patched for Win10
(c framework is included)

----------

Third party code
-------------

GenRes, to handle maximum compatibility, uses third party code and lib:

 - [FreeImage](http://freeimage.sourceforge.net/) 
 - [Sound eXchange](http://sox.sourceforge.net/)
 - [ST-Sound Library](http://leonard.oxg.free.fr/StSoundGPL.html) by Arnaud Carré
 - [Mappy](http://www.tilemap.co.uk/mappy.php) library

----------

Developer
-------------

Want to clone, fork, whatever ?
To fix an issue or to add a plugin ?
To make your own &lt;insert your console name here&gt;Res ?
Go ahead !
No licence headache, do as you wish !
Just don't sell it, please.

For more info, please read `README.build` file and docs in lib folder.

